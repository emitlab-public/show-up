package main

import (
	"dnl_server/methods/notify"
	"dnl_server/methods/users"
	"dnl_server/utils"

	jsonrpc "github.com/DizoftTeam/jsonrpc_server"
	"github.com/gorilla/websocket"
	_ "github.com/mattn/go-sqlite3"
	"github.com/nats-io/nats.go"

	"fmt"
	"log"
	"net/http"
)

const (
	url = ":8089"
)

var (
	wsUpgrade = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	} // use default options
)

// Регистрация методов и обработчиков
func registerMethods() {
	// Users

	jsonrpc.Register("users.login", users.Login{})

	// Notify

	jsonrpc.Register("notify.create", notify.Create{})
	jsonrpc.Register("notify.list", notify.List{})

	// Filters

	// Other

	jsonrpc.RegisterFunc("version", func(params interface{}) (interface{}, *jsonrpc.RPCError) {
		return "1.0.1", nil
	})
}

// WebSocket handler
func wsHandler(w http.ResponseWriter, r *http.Request) {
	login := r.URL.Query().Get("login")
	password := r.URL.Query().Get("password")

	uc := users.UserService{}

	userID, err := uc.Login(login, password)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(`{"success": false, "error": "wrong auth info"}`))

		log.Println("Wrong auth: l(", login, "), p(", password, ")")

		return
	}

	c, err := wsUpgrade.Upgrade(w, r, nil)

	if err != nil {
		log.Println("Cant upgrade:" + err.Error())

		return
	}

	conn := utils.ConnectToNats()
	defer conn.Close()

	ch := make(chan *nats.Msg)

	sub, _ := conn.ChanSubscribe(
		fmt.Sprintf("notify.%s.create", userID),
		ch,
	)

	for {
		d := <-ch

		if !d.Sub.IsValid() {
			break
		}

		err := c.WriteMessage(websocket.TextMessage, d.Data)

		// Если есть ошибка, скорей всего клиент ушел
		if err != nil {
			log.Println("WSClient was left")

			break
		}
	}

	if sub.IsValid() {
		sub.Unsubscribe()
	}

	_ = c.Close()
}

func main() {
	addr := utils.GetCmdArg("addr", url).(string)

	registerMethods()

	http.HandleFunc("/", jsonrpc.Handler)
	http.HandleFunc("/ws", wsHandler)

	fmt.Println()
	log.Printf("Starting server at %v\n\n", addr)

	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Panic("Cant start server", err)
	}
}
