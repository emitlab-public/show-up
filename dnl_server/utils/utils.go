package utils

import (
	"encoding/json"

	jsonrpc "github.com/DizoftTeam/jsonrpc_server"
	"github.com/jmoiron/sqlx"
	"github.com/nats-io/nats.go"

	"log"
	"os"
	"strings"
)

const (
	dbName = "./dnl_db.db"
)

// Object typedef
type Object map[string]interface{}

// GetCmdArg Получение аргумента командной строки
// Вернет nil если ничего не нашел
func GetCmdArg(name string, defaultValue interface{}) interface{} {
	args := os.Args[1:]

	for _, arg := range args {
		parts := strings.Split(arg, "=")
		cmdName, cmdVal := parts[0], parts[1]

		// Если в имени есть совпадение
		if strings.Contains(cmdName, name) {
			return cmdVal
		}
	}

	return defaultValue
}

// ConnectToDb Подключение к ДБ
func ConnectToDb() *sqlx.DB {
	db, err := sqlx.Connect(
		"sqlite3",
		GetCmdArg("dbname", dbName).(string),
	)

	if err != nil {
		log.Panicln("Cant connect to db:", err)
	}

	return db
}

// ConnectToNats Возвращает объект для работы с NATS
func ConnectToNats() *nats.Conn {
	conn, err := nats.Connect(
		GetCmdArg("nats-addr", "nats://dnl_nats:4222").(string), // TODO: in ENV
	)

	if err != nil {
		log.Panicln("Cant connect to NATS:", err)
	}

	return conn
}

// NatsPublish Публикация сообщения в NATS
func NatsPublish(subject string, data interface{}) {
	conn := ConnectToNats()
	defer conn.Close()

	jData, _ := json.Marshal(data)

	conn.Publish(subject, jData)
}

// AuthError Ошибка: пользователь не авторизован
func AuthError() (interface{}, *jsonrpc.RPCError) {
	return nil, &jsonrpc.RPCError{
		Code:    -10,
		Message: "Auth required",
	}
}
