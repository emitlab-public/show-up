package users

import (
	jsonrpc "github.com/DizoftTeam/jsonrpc_server"
	"github.com/mitchellh/mapstructure"
)

// Login Структура для метода
type Login struct {
}

// Получаемые данные
type data struct {
	User User `mapstructure:"user"`
}

// Handler Тело метода
func (l Login) Handler(params interface{}) (interface{}, *jsonrpc.RPCError) {
	rData := data{}

	if err := mapstructure.Decode(params, &rData); err != nil {
		return nil, &jsonrpc.RPCError{
			Code:    -2000,
			Message: "Ошибка в данных авторизации",
		}
	}

	if rData.User.Username == "" || rData.User.Password == "" {
		return nil, &jsonrpc.RPCError{
			Code:    -2010,
			Message: "Отсутствуют необходимые данные!",
		}
	}

	uc := UserService{}

	userID, err := uc.Login(rData.User.Username, rData.User.Password)

	if err != nil {
		// TODO: check dev version and prod

		return nil, &jsonrpc.RPCError{
			Code:    -2010,
			Message: "Ошибка получения данных о пользователе: " + err.Error(),
		}
	}

	return userID, nil
}
