package users

import (
	"errors"

	"dnl_server/utils"
)

// UserService Сервис для работы с пользователями
type UserService struct{}

// Login Авторизует пользователя и возвращает его идентификатор
func (uc *UserService) Login(login string, password string) (string, error) {
	db := utils.ConnectToDb()
	defer db.Close()

	user := User{}

	if err := db.Get(
		&user,
		"SELECT * FROM users WHERE username = $1 AND password = $2",
		login,
		password,
	); err != nil {
		// TODO: check dev version and prod
		return "", errors.New("Wrong login or password")
	}

	return user.ID, nil
}
