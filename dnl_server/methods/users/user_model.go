package users

type User struct {
	ID       string `json:"id"                               db:"id"`
	Username string `json:"username" mapstructure:"login"    db:"username"`
	Password string `json:"password" mapstructure:"password" db:"password"`
}
