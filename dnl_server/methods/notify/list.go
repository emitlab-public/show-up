package notify

import (
	"dnl_server/utils"
	"log"

	jsonrpc "github.com/DizoftTeam/jsonrpc_server"
	"github.com/mitchellh/mapstructure"
)

/*
List Структура метода

Пример запроса

```json
{
  "jsonrpc": "2.0",
  "method": "notify.list",
  "params": {
    "user": "176ca597-8816-4d29-b215-ba7d7dcdbe96"
  },
  "id": 1
}
```

*/
type List struct{}

// Получаемые данные
type listData struct {
	User string `mapstructure:"user"`
}

// Handler Тело метода
func (n List) Handler(params interface{}) (interface{}, *jsonrpc.RPCError) {
	rData := listData{}

	if err := mapstructure.Decode(params, &rData); err != nil {
		return nil, &jsonrpc.RPCError{
			Code:    -4000,
			Message: "Ошибка в данных",
		}
	}

	db := utils.ConnectToDb()
	defer db.Close()

	if rData.User == "" {
		return utils.AuthError()
	}

	notifications := []Notification{}

	query := "SELECT * FROM notifications WHERE user_id = $1;"

	// db.Select(&notifications, "SELECT * FROM notifications WHERE user_id = $1", rData.User)
	if err := db.Select(&notifications, query, rData.User); err != nil {
		log.Println("Ошибка запроса на список нотификаций", err)

		return nil, &jsonrpc.RPCError{
			Code:    -4010,
			Message: "Ошибка базы данных",
		}
	}

	result := []utils.Object{}

	for _, it := range notifications {
		result = append(result, utils.Object{
			"package_name": it.PackageName,
			"title":        it.Title,
			"text":         it.Text,
			"id":           it.NotifyID,
			"gid":          it.ID,
			"created":      it.Created,
			"user_id":      it.User,
		})
	}

	return result, nil
}
