package notify

// Notification Модель нотификации
type Notification struct {
	PackageName string `json:"packageName" mapstructure:"packageName" db:"package_name"`
	Title       string `json:"title"       mapstructure:"title"       db:"title"`
	Text        string `json:"text"        mapstructure:"text"        db:"text"`
	NotifyID    int    `json:"id"          mapstructure:"id"          db:"notify_id"`
	ID          int    `                                              db:"id"`
	Created     string `json:"created"                                db:"created"`
	User        string `json:"userId"                                 db:"user_id"`
}
