package notify

import (
	"dnl_server/utils"
	"fmt"

	jsonrpc "github.com/DizoftTeam/jsonrpc_server"
	"github.com/mitchellh/mapstructure"
)

// Create Структура для метода
type Create struct {
}

// Получаемые данные
type createData struct {
	Notify Notification `mapstructure:"notification"`
	User   string       `mapstructure:"user"`
}

// Handler Тело метода
func (n Create) Handler(params interface{}) (interface{}, *jsonrpc.RPCError) {
	rData := createData{}

	if err := mapstructure.Decode(params, &rData); err != nil {
		return nil, &jsonrpc.RPCError{
			Code:    -1000,
			Message: "Ошибка в данных оповещения",
		}
	}

	db := utils.ConnectToDb()
	defer db.Close()

	if rData.User == "" {
		return utils.AuthError()
	}

	if rData.Notify.PackageName == "" || rData.Notify.NotifyID == 0 {
		return jsonrpc.EmptyRequestError()
	}

	insertData := map[string]interface{}{
		"package_name": rData.Notify.PackageName,
		"title":        rData.Notify.Title,
		"text":         rData.Notify.Text,
		"notify_id":    rData.Notify.NotifyID,
		"user_id":      rData.User,
	}

	// TODO: Need filter

	if _, err := db.NamedExec(
		"INSERT INTO notifications (package_name, title, text, notify_id, user_id) VALUES (:package_name, :title, :text, :notify_id, :user_id)",
		insertData,
	); err != nil {
		return nil, &jsonrpc.RPCError{
			Code:    -1010,
			Message: "Не удалось добавить запись: " + err.Error(),
		}
	}

	utils.NatsPublish(
		fmt.Sprintf("notify.%s.create", rData.User),
		insertData,
	)

	return true, nil
}
