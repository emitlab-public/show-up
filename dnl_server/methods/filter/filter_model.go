package filter

// NotificationFilter Модель фильтра
type NotificationFilter struct {
	ID    int    `db:"id"    json:"id"`
	Value string `db:"value" json:"value"`
	Title string `db:"title" json:"title"`
}
