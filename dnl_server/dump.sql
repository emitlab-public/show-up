DROP INDEX IF EXISTS idx_users_username;
DROP INDEX IF EXISTS idx_notification_filter_name;

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS notification_filter;
DROP TABLE IF EXISTS notifications;

-- Users

CREATE TABLE IF NOT EXISTS users
(
    id       TEXT NOT NULL PRIMARY KEY,
    username TEXT NOT NULL,
    password TEXT NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_users_username ON users (username);

-- Filter

CREATE TABLE IF NOT EXISTS notification_filter
(
    id      INTEGER PRIMARY KEY AUTOINCREMENT,
    value   TEXT NOT NULL,
    title   TEXT,
    type    TEXT NOT NULL,
    user_id INTEGER REFERENCES users (id)
);

CREATE INDEX IF NOT EXISTS idx_notification_filter_name ON notification_filter (value);

-- Notify

CREATE TABLE IF NOT EXISTS notifications
(
    id           INTEGER PRIMARY KEY AUTOINCREMENT,
    created      TEXT DEFAULT (datetime('now')),
    package_name TEXT NOT NULL,
    title        TEXT,
    text         TEXT,
    notify_id    INTEGER,
    user_id      INTEGER REFERENCES users (id)
);

-- -------------------------------------------
--                  Данные
-- -------------------------------------------

-- Пользователь
INSERT INTO users (id, username, password)
VALUES ('176ca597-8816-4d29-b215-ba7d7dcdbe96', 'wiright', '2580');
