module dnl_server

go 1.14

require (
	github.com/DizoftTeam/jsonrpc_server v1.0.0
	github.com/gorilla/websocket v1.4.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.14.0
	github.com/mitchellh/mapstructure v1.3.3
	github.com/nats-io/nats-server/v2 v2.1.7 // indirect
	github.com/nats-io/nats.go v1.10.0
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
