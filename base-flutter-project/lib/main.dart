import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:get_storage/get_storage.dart';
import 'package:layout/layout.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'common/resources.dart';
import 'screens/auth/auth_screen.dart';
import 'screens/home/home_screen.dart';
import 'screens/splash/splash_screen.dart';
import 'services/auth_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  GetIt.I.registerSingleton<AuthService>(AuthService());

  final List<String> storageNames = <String>[
    Const.storageAuth,
  ];

  for (String storageName in storageNames) {
    await GetStorage.init(storageName);
  }

  runApp(const MyApp());
}

///
/// Основной класс приложения
///
class MyApp extends StatefulWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

///
/// Состояние приложения
///
class _MyAppState extends State<MyApp> {
  /// Ключ навигатора
  final GlobalKey<NavigatorState> _navKey = GlobalKey();

  ///
  /// Функция для создания билдера роута
  ///
  WidgetBuilder _route(Widget child) => (_) => child;

  @override
  void initState() {
    GetIt.I.registerSingleton<GlobalKey<NavigatorState>>(
      _navKey,
      instanceName: 'globalNavKey',
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      <DeviceOrientation>[
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
    );

    return OverlaySupport.global(
      child: Layout(
        child: Builder(
          builder: (BuildContext context) {
            return ScreenUtilInit(
              designSize: const Size(375, 815),
              minTextAdapt: true,
              splitScreenMode: true,
              builder: (_, __) => MaterialApp(
                title: 'Tesmit',
                theme: ThemeData(
                  primarySwatch: Colors.indigo,
                  visualDensity: VisualDensity.adaptivePlatformDensity,
                ),
                debugShowCheckedModeBanner: true,
                localizationsDelegates: AppLocalizations.localizationsDelegates,
                supportedLocales: AppLocalizations.supportedLocales,
                initialRoute: SplashScreen.route,
                routes: <String, WidgetBuilder>{
                  SplashScreen.route: _route(const SplashScreen()),
                  AuthScreen.route: _route(const AuthScreen()),
                  HomeScreen.route: _route(const HomeScreen()),
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
