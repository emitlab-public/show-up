import 'package:flutter/material.dart';

import '../../widgets/wspace.dart';

///
/// Экран авторизации
///
class AuthScreen extends StatefulWidget {
  /// Имя роута
  static const String route = '/auth';

  ///
  /// Экран авторизации
  ///
  const AuthScreen({
    Key? key,
  }) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

///
/// Состояние экрана
///
class _AuthScreenState extends State<AuthScreen> {
  // Ключ доступа к форме
  final GlobalKey<FormState> _form = GlobalKey();

  // Контроллеры для работы с полями ввода
  final TextEditingController _login = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Авторизация'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 400,
              child: Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Form(
                    key: _form,
                    child: Column(
                      children: <Widget>[
                        // Логин
                        TextFormField(
                          controller: _login,
                          decoration: const InputDecoration(
                            labelText: 'Логин',
                          ),
                        ),
                        // Пароль
                        TextFormField(
                          controller: _password,
                          decoration: const InputDecoration(
                            labelText: 'Пароль',
                          ),
                        ),
                        const HSpace(16),
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: const Text('Войти'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
