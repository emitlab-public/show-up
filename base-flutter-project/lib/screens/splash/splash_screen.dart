import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../core/extensions/context.dart';
import '../../screens/home/home_screen.dart';
import '../../services/auth_service.dart';
import '../../widgets/wspace.dart';
import '../auth/auth_screen.dart';

///
/// Экран заставки
///
class SplashScreen extends StatefulWidget {
  /// Имя роута
  static const String route = '/';

  /// Флаг что надо делать автонавигацию
  final bool autoNavigate;

  const SplashScreen({
    Key? key,
    this.autoNavigate = true,
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

///
/// Состояние экрана
///
class _SplashScreenState extends State<SplashScreen> {
  /// Сервисы
  final AuthService _authService = GetIt.I.get<AuthService>();

  ///
  /// Навигация на основной экран
  ///
  void _navigate() async {
    if (widget.autoNavigate) {
      final bool isAuth = await _authService.isAuth;

      Navigator.of(context).pushReplacementNamed(
        isAuth ? HomeScreen.route : AuthScreen.route,
      );
    }
  }

  @override
  void initState() {
    Future<void>.delayed(const Duration(seconds: 1), _navigate);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 3,
                ),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(
              width: 150,
              height: 120,
              child: FittedBox(
                child: Text(
                  context.locale.appName,
                  style: GoogleFonts.dosis(),
                ),
              ),
            ),
            const HSpace(16),
            // const CircularProgressIndicator(),
            SizedBox(
              width: 250,
              height: 40,
              child: SvgPicture.asset(
                'assets/svg/Line.svg',
                fit: BoxFit.contain,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
