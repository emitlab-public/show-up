import 'package:flutter/material.dart';

///
/// Основной экран приложения
///
class HomeScreen extends StatefulWidget {
  /// Роут
  static const String route = '/home';

  const HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

///
/// Состояние экрана
///
class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (BuildContext context) {
          return const SafeArea(
            child: Center(
              child: Text('Main Screen'),
            ),
          );
        },
      ),
    );
  }
}
