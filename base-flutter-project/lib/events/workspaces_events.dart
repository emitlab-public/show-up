///
/// Пример события создания новой сущности
///
class WorkspaceCreateEvent {
  final String workspace;

  WorkspaceCreateEvent({
    required this.workspace,
  });
}
