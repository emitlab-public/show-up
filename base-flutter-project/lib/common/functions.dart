import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../core/widgets/floating_modal.dart';

///
/// Глобальный навигатор
///
NavigatorState get globalNavigator {
  return GetIt.I
      .get<GlobalKey<NavigatorState>>(
        instanceName: 'globalNavKey',
      )
      .currentState!;
}

///
/// Показ диалога как Float
///
Future<T?> showFloatingModalBottomSheet<T>({
  required BuildContext context,
  required WidgetBuilder builder,
  Color? backgroundColor,
}) {
  return showCustomModalBottomSheet<T>(
    context: context,
    builder: builder,
    useRootNavigator: true,
    containerWidget: (_, __, Widget child) {
      return FloatingModal(
        child: child,
      );
    },
    expand: false,
  );
}
