import 'package:event_bus/event_bus.dart';

export '../events/workspaces_events.dart';

/// Глобальная шина данных [EventBus]
EventBus eventBus = EventBus();
