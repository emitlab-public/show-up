///
/// Сервис со вспомогательными функциями
///
class Utils {
  ///
  /// Склонение числительных
  ///
  /// [number] Число, по которому будем склонять [int]
  /// [titles] Возможные наборы данных. Должно быть 3 варианта [List<String>]
  ///
  /// Пример:
  ///
  /// ```dart
  ///   print(HelperService.declOfNum(1, ['секунда', 'секунды', 'секунд'])); // секунда
  ///   print(HelperService.declOfNum(2, ['секунда', 'секунды', 'секунд'])); // секунды
  ///   print(HelperService.declOfNum(5, ['секунда', 'секунды', 'секунд'])); // секунд
  /// ```
  ///
  static T declOfNum<T>(int number, List<T> titles) {
    List<int> cases = <int>[2, 0, 1, 1, 1, 2];

    return titles[(number % 100 > 4 && number % 100 < 20)
        ? 2
        : cases[(number % 10 < 5) ? number % 10 : 5]];
  }
}

typedef ValidatorFunc = String? Function(String? value);

class Validators {
  ///
  /// Комбинирование нескольких валидаторов
  /// Исполнение идет в порядке их передачи
  ///
  static String? combine(List<ValidatorFunc> validators, String? value) {
    for (ValidatorFunc vfunc in validators) {
      final String? result = vfunc.call(value);

      if (result != null) {
        return result;
      }
    }

    return null;
  }

  ///
  /// Метод валидации данных
  ///
  static String? string(String? value) {
    if (value == null || value.isEmpty) {
      return 'Значение не может быть пустым';
    }

    if (value.length < 3) {
      return 'Значение не может быть меньше 3 символов';
    }

    return null;
  }
}
