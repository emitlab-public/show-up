import 'package:flutter/widgets.dart';

///
/// Описание возможных тем приложения
///
enum AppThemes {
  ///
  /// Оранжевая тема
  ///
  /// Основной цвет #2D74FF
  ///
  BLUE,
}

///
/// Основной класс темы приложения
///
abstract class AppTheme {
  ///
  /// Базовый конструктор
  ///
  AppTheme();

  ///
  /// Создание экземпляра темы приложения
  ///
  factory AppTheme.of(BuildContext context, AppThemes type) {
    switch (type) {
      case AppThemes.BLUE:
        return BlueTheme();
    }
  }

  ///
  /// Получение основого цвета
  ///
  Color get primary;

  ///
  /// Получение дополнительного цвета
  ///
  Color get accent;

  ///
  /// Цвет текста
  ///
  Color get text;

  ///
  /// Цвет заднего фона
  ///
  Color get bg;
}

class BlueTheme extends AppTheme {
  @override
  Color get primary => const Color(0xFF2D74FF);

  @override
  Color get accent => const Color(0xFFF4D6C6);

  @override
  Color get bg => const Color(0xFFFBF5F2);

  @override
  Color get text => const Color(0xFF120D26);
}
