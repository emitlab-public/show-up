import 'package:intl/intl.dart';

///
/// Константы
///
abstract class Const {
  ///
  /// Хранилище авторизации
  ///
  static const String storageAuth = 'auth';
}

///
/// Высчитываемые константы
///
abstract class Compute {
  ///
  /// Денежный форматтер
  ///
  static final NumberFormat currency = NumberFormat.currency(
    locale: 'ru_RU',
    symbol: 'руб.',
    decimalDigits: 0,
  );
}

///
/// Имена ключей хранилищ
///
abstract class StorageKeys {
  ///
  /// Ключ хранения токена авторизации
  ///
  static const String authToken = 'auth.token';
}

///
/// Указатели на файлы ассетов
///
abstract class Assets {
  ///
  /// Логотип
  ///
  static const String logo = 'assets/images/logo.png';
}

///
/// Описание некоторых значений по-умолчанию
///
abstract class Defaults {
  ///
  /// Длительность анимаций
  ///
  static const Duration animationDur = Duration(milliseconds: 300);
}

///
/// Цвета приложения
///
abstract class AppColors {}
