import 'package:get_storage/get_storage.dart';

import '../common/resources.dart';

///
/// Хранилище авторизации
///
final GetStorage authStorage = GetStorage(Const.storageAuth);
