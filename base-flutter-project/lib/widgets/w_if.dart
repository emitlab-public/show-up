import 'package:flutter/material.dart';

///
/// Виджет условной отрисовки
///
class Wif extends StatelessWidget {
  /// Условие по которому будет происходить отрисовка
  final bool condition;

  /// Построение содержимого
  final WidgetBuilder builder;

  /// Виджет если условие не удовлетворительно
  final WidgetBuilder? fallback;

  ///
  /// Виджет условной отрисовки
  ///
  const Wif({
    Key? key,
    required this.condition,
    required this.builder,
    this.fallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return condition
        ? builder(context)
        : fallback != null
            ? fallback!(context)
            : Offstage();
  }
}
