import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../core/extensions/context.dart';

///
/// Типы шрифтов
///
enum TypographyType {
  BodyRegular12px,

  BodyBold12px,

  HeadBold20px,
}

///
/// Расширение для типов шрифтов
///
extension TypographyTypeExtension on TypographyType {
  ///
  /// Получение [TextStyle] для каждого типа шрифта
  ///
  TextStyle style([BuildContext? context]) {
    switch (this) {
      case TypographyType.BodyRegular12px:
        return GoogleFonts.nunito(
          fontWeight: FontWeight.w400,
          fontSize: 12.sp,
          height: 1.16,
        );

      case TypographyType.BodyBold12px:
        return GoogleFonts.nunito(
          fontWeight: FontWeight.w700,
          fontSize: 12.sp,
          height: 1.33,
        );

      case TypographyType.HeadBold20px:
        return GoogleFonts.nunito(
          fontWeight: FontWeight.w700,
          fontSize: 20.sp,
          height: 1.36,
        );
    }
  }
}

///
/// Виджет для отображения текста в приложении
///
/// Тесно использует [TypographyType]
/// На его основе выбирается стиль текста
///
/// Важно: планшетная верстка не учитывается в стилях - меняется сам стиль,
/// поэтому контролировать нужно извне
///
class AppTypography extends StatelessWidget {
  /// Текст
  final String text;

  /// Стиль текста
  final TypographyType type;

  /// Цвет текста
  final Color color;

  /// Кол-во линий
  final int? maxLines;

  /// Направление текста
  final TextAlign textAlign;

  /// Жирность шрифта
  final FontWeight? fontWeight;

  /// Overflow
  final TextOverflow? overflow;

  /// Межстрочный интервал
  final double? height;

  const AppTypography(
    this.text, {
    Key? key,
    this.type = TypographyType.BodyBold12px,
    this.color = Colors.black,
    this.maxLines = 1,
    this.textAlign = TextAlign.left,
    this.fontWeight,
    this.overflow = TextOverflow.ellipsis,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: overflow,
      style: type.style(context).copyWith(
            color: color,
            fontWeight: fontWeight,
            height: height,
          ),
    );
  }
}
