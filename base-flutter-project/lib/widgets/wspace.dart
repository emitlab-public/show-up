import 'package:flutter/widgets.dart';

import '../core/extensions/context.dart';

///
/// Разделитель между виджетами
///
class HSpace extends StatelessWidget {
  /// Высота разделителя
  final double height;

  /// Флаг что необходимо использовать [SliverToBoxAdapter]
  final bool useSliver;

  ///
  /// Разделитель между виджетами
  ///
  const HSpace(
    this.height, {
    Key? key,
  })  : useSliver = false,
        super(key: key);

  const HSpace.sliver(
    this.height, {
    Key? key,
  })  : useSliver = true,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget child = SizedBox(
      height: height.h,
    );

    if (useSliver) {
      child = SliverToBoxAdapter(
        child: child,
      );
    }

    return child;
  }
}

///
/// Разделитель между виджетами
///
class WSpace extends StatelessWidget {
  /// Ширина разделителя
  final double width;

  /// Флаг что необходимо использовать [SliverToBoxAdapter]
  final bool useSliver;

  ///
  /// Разделитель между виджетами
  ///
  const WSpace(
    this.width, {
    Key? key,
  })  : useSliver = false,
        super(key: key);

  const WSpace.sliver(
    this.width, {
    Key? key,
  })  : useSliver = true,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget child = SizedBox(
      width: width.w,
    );

    if (useSliver) {
      child = SliverToBoxAdapter(
        child: child,
      );
    }

    return child;
  }
}
