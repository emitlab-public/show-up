import 'package:curl_logger_dio_interceptor/curl_logger_dio_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../components/env.dart';

///
/// Обработчик на события для авторизации
///
InterceptorsWrapper get _auth {
  return InterceptorsWrapper(
    onRequest: (RequestOptions options, RequestInterceptorHandler handler) {
      options.headers.putIfAbsent(
        'X-Auth-Token',
        () => 'af8c9a59-0cac-4f53-a268-95cc00237c0f',
      );

      return handler.next(options);
    },
  );
}

///
/// Обработчик на события для формирования ответа
///
InterceptorsWrapper get _response {
  return InterceptorsWrapper(
    onResponse: (
      Response<dynamic> response,
      ResponseInterceptorHandler handler,
    ) {
      debugPrint(
        'Response [${response.statusCode}] '
        '{${response.requestOptions.path}}',
      );

      if (response.data is Map<String, dynamic>) {
        final Map<String, dynamic> rData = Map<String, dynamic>.from(
          response.data,
        );

        if (rData.containsKey('success') &&
            rData['success'] &&
            rData.containsKey('result')) {
          try {
            final Map<String, dynamic> result = Map<String, dynamic>.from(
              rData['result'],
            );

            response.data = result;
          } catch (_) {
            final List<dynamic> result = List<dynamic>.from(rData['result']);

            response.data = result;
          }

          handler.next(response);

          return;
        }
      }

      return handler.next(response);
    },
  );
}

///
/// API клиент для работы с бекендом
///
Dio get apiClient {
  final Dio client = Dio(
    BaseOptions(
      baseUrl: Env.get.url.toString(),
      contentType: 'application/json',
    ),
  );

  client.interceptors.add(_auth);
  client.interceptors.add(PrettyDioLogger());
  client.interceptors.add(CurlLoggerDioInterceptor());
  // client.interceptors.add(_response);

  return client;
}
