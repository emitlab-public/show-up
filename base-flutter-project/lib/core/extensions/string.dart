///
/// Расширение для работы со строками
///
extension MString on String {
  ///
  /// Слово с заглавной буквы
  ///
  String capitalyze() {
    final String str = toLowerCase();
    final String first = str.substring(0, 1);

    return '${first.toUpperCase()}${str.substring(1)}';
  }
}
