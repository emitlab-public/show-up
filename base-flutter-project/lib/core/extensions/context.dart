import 'package:flutter/widgets.dart';
import 'package:layout/layout.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// Подключение сторонней библиотеки
/// Сделано для упрощения чтения импортов
export 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///
/// Маштабирование в зависимости от контекста
///
extension ScaleFromContext on BuildContext {
  double layoutWidth({
    required double xs,
    double sm = 0,
  }) {
    return layout.value(
      xs: xs.w,
      sm: sm.w,
    );
  }

  double layoutHeight({
    required double xs,
    double sm = 0,
  }) {
    return layout.value(
      xs: xs.h,
      sm: sm.h,
    );
  }

  double layoutRadius({
    required double xs,
    double sm = 0,
  }) {
    return layout.value(
      xs: xs.r,
      sm: sm.r,
    );
  }

  ///
  /// Screen Width
  ///
  double get sw => MediaQuery.of(this).size.width;

  ///
  /// Screen Height
  ///
  double get sh => MediaQuery.of(this).size.height;
}

///
/// Расширение для работы с локализацией
///
extension LocalContext on BuildContext {
  ///
  /// Получение локализации
  ///
  AppLocalizations get locale => AppLocalizations.of(this)!;
}
