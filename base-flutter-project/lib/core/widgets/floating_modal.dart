import 'package:flutter/material.dart';

import '../../core/extensions/context.dart';

class FloatingModal extends StatelessWidget {
  final Widget child;
  final Color? backgroundColor;

  const FloatingModal({
    Key? key,
    required this.child,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double paddingValue = 20.r;

    return SafeArea(
      child: Padding(
        padding: EdgeInsets.only(
          left: paddingValue,
          right: paddingValue,
          bottom: paddingValue,
        ),
        child: Material(
          color: backgroundColor,
          clipBehavior: Clip.antiAlias,
          borderRadius: BorderRadius.all(
            Radius.circular(18.r),
          ),
          child: child,
        ),
      ),
    );
  }
}
