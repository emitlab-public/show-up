import 'package:intl/intl.dart';

///
/// Расширение даты
///
extension ModelDateExtension on DateTime {
  ///
  /// Перевод даты в строку для JSON
  ///
  String? get toJsonDate {
    return '${day.toString().padLeft(4, '0')}-'
        '${month.toString().padLeft(2, '0')}-'
        '${year.toString().padLeft(2, '0')}';
  }
}

///
/// Миксин для моделей
///
mixin ModelMixin {
  ///
  /// Перевод строки даты в [DateTime] для модели
  ///
  static DateTime? toModelDate(String? date) {
    if (date == null) {
      return null;
    }

    return DateFormat('dd.MM.yyyy HH:mm:ss').parse(date);
  }

  ///
  /// Перевод даты в строку для JSON
  ///
  String? toJsonDate(DateTime? date) {
    if (date == null) {
      return null;
    }

    return '${date.day.toString().padLeft(4, '0')}-'
        '${date.month.toString().padLeft(2, '0')}-'
        '${date.year.toString().padLeft(2, '0')}';
  }
}
