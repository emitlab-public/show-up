import 'dart:async';

import 'package:flutter/widgets.dart';

///
/// Миксин для виджетов которые имеют подписки
///
/// Автоматически отписывается в методе [dispose]
///
mixin Subscriptionable<T extends StatefulWidget> on State<T> {
  /// Массив подписок
  final List<StreamSubscription<dynamic>> subs =
      <StreamSubscription<dynamic>>[];

  ///
  /// Метод получения списка подписок
  ///
  List<StreamSubscription<dynamic>> get subscribe {
    return <StreamSubscription<dynamic>>[];
  }

  ///
  /// Обновление состояния экрана если он [mounted]
  ///
  @protected
  void setState_(VoidCallback? callback) {
    callback?.call();

    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    subs.addAll(subscribe);

    super.initState();
  }

  @override
  void dispose() {
    for (StreamSubscription<dynamic> sub in subs) {
      sub.cancel();
    }

    super.dispose();
  }
}

///
/// Миксин для подписки любых классов
///
mixin WithSubscription on Object {
  /// Массив подписок
  final List<StreamSubscription<dynamic>> subs =
      <StreamSubscription<dynamic>>[];

  ///
  /// Метод получения списка подписок
  ///
  List<StreamSubscription<dynamic>> get subscribe {
    return <StreamSubscription<dynamic>>[];
  }

  ///
  /// Добавить все подписки из subscribe
  ///
  void subscribeAll() {
    subs.addAll(subscribe);
  }

  ///
  /// Отписаться от всех подписок
  ///
  void unsubscribe() {
    for (StreamSubscription<dynamic> sub in subs) {
      sub.cancel();
    }
  }
}
