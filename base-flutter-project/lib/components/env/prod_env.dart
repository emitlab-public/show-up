import '../env.dart';

///
/// Продакшн сборка
///
class ProdEnv extends Env {
  @override
  Uri get url => Uri.parse('https://api.emitlab.ru');

  @override
  String get mode => 'prod';
}
