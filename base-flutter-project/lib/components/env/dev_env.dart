import '../env.dart';

///
/// DEV сборка
///
class DevEnv extends Env {
  @override
  Uri get url => Uri.parse('https://api.emitlab.ru');

  @override
  String get mode => 'dev';
}
