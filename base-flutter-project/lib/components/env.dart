import 'package:flutter/foundation.dart' show kDebugMode;

import 'env/dev_env.dart';
import 'env/prod_env.dart';

///
/// Базовый класс ENV
///
abstract class Env {
  ///
  /// Указание на URL бекенда
  ///
  Uri get url;

  ///
  /// Режим работы приложения
  ///
  String get mode;

  ///
  /// Получение ENV более удобным для написания кода способом
  ///
  static Env get get {
    return Env.of();
  }

  ///
  /// Получение ENV
  ///
  static Env of() {
    if (!kDebugMode) {
      return ProdEnv();
    }

    return DevEnv();
  }
}
