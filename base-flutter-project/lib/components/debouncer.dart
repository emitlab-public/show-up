import 'dart:async';

import 'package:flutter/material.dart';

///
/// Компонент для выполнения отложенных операций
///
/// Пример использования
///
/// ```dart
/// final Debouncer _searchDelayer = Debouncer(
///   delay: const Duration(milliseconds: 300),
/// );
///
/// Widget _buildButton(BuildContext) {
///   return ElevatedButton(
///     onPressed: () {
///       _debouncer.run(() {
///         debugPrint('Type your code here');
///       });
///     },
///   );
/// }
/// ```
///
class Debouncer {
  /// Время через которое необходимо вызывать функцию
  final Duration delay;

  /// Таймер
  Timer? _timer;

  Debouncer({
    required this.delay,
  });

  ///
  /// Запуск отложенного события
  ///
  void run(VoidCallback action) {
    _timer?.cancel();

    _timer = Timer(delay, action);
  }
}
