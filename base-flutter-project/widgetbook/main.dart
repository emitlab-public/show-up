import 'package:base_project/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'widgetbook.dart';

void main() {
  GetIt.I.registerSingleton<AuthService>(AuthService());

  runApp(const HotReload());
}
