import 'package:base_project/core/extensions/context.dart';
import 'package:base_project/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:layout/layout.dart';
import 'package:widgetbook/widgetbook.dart';

class HotReload extends StatelessWidget {
  const HotReload({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 815),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (_, __) => Layout(
        child: Widgetbook.material(
          appInfo: AppInfo(
            name: 'Imovito',
          ),
          devices: <Device>[
            Apple.iPhone12Mini,
            Samsung.s21ultra,
          ],
          themes: <WidgetbookTheme<ThemeData>>[
            WidgetbookTheme<ThemeData>(
              name: 'Light',
              data: ThemeData.light(),
            ),
          ],
          categories: <WidgetbookCategory>[
            WidgetbookCategory(
              name: 'Screens',
              widgets: <WidgetbookComponent>[
                WidgetbookComponent(
                  name: 'Splash',
                  useCases: <WidgetbookUseCase>[
                    WidgetbookUseCase(
                      name: 'Splash Screen',
                      builder: (_) => const SplashScreen(
                        autoNavigate: false,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
