# base_project

Базовый Flutter проект для быстрого разворачивания проектов

## Для начала

Данный проект является стартовой точкой для новых проектов

### Как пользоваться

Необходимо выполнить несколько простых шагов

> Примечание: Имя нового проекта берем как `tesmit`

- Склонировать репозиторий с указанием названия папки как новый проект

```bash
git clone git@gitlab.com:emitlab/deploy/base-flutter-project.git tesmit
```

- Удалить папку `.git`

```bash
rm -rf .git
```

- По новой инициализировать git и провести базовую настройку

```bash
git init --initial-branch=master
git remote add origin git@gitlab.com:emitlab/tesmit.git
git add .
git commit -m 'Initial commit'
git push -u origin master
```

- Внедрять функционал необходимый по ТЗ

## Что нужно подправить

- Сменить название проекта в `pubspec.yaml` -> name
- Указать URL в `lib/core/services/api_client.dart`
- Переименовать файл base_project.iml
- Удалить папку android и выполнить команду 

```bash
flutter create --platforms android .
```

- Удалить данное содержимое и написать необходимый README

### Не тестировалось

- Использование пакета [rename](https://pub.dev/packages/rename)