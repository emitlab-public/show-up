<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Exception\DatabaseException;
use App\Repository\TemplateRepository;
use App\Tests\UnitTester;
use Codeception\Example;
use Codeception\Exception\ConfigurationException;
use Throwable;

/**
 * Тестирование базовых методов выборки
 *
 * Тестируются методы:
 *      find
 *      findAll // Он тестируется в методе findBy (см. реализацию findAll)
 *      findOneBy
 *      findBy
 *      count
 *
 * @package App\Tests\unit
 */
class AbstractRepositoryCest extends AbstractUnitCest
{
    /** @var TemplateRepository */
    private $_repository;

    /**
     * @inheritDoc
     * @param UnitTester $I
     * @throws ConfigurationException
     */
    public function _before(UnitTester $I): void
    {
        parent::_before($I);

        // Не важно какой репозиторий, они все являются наследниками AbstractRepository
        $this->_repository = $this->_container->get(TemplateRepository::class);
    }

    /**
     * Тестирование метода find
     *
     * @param UnitTester $I
     * @param Example $example
     *
     * @example { "id": "guid" }
     * @example { "id": "1" }
     * @example { "id": 1 }
     * @example { "id": "_)(*&^%$#@" }
     * @example { "id": "<?php echo 'guid';>" }
     * @example { "id": "null" }
     * @example { "id": null }
     * @example { "id": 88005553535 }
     * @example { "id": "8-800-555-35-35" }
     * @example { "id": "8(800)555-35-35" }
     * @example { "id": "    " }
     * @example { "id": "" }
     * @example { "id": "[]" }
     * @example { "id": "<pre>Белеет парус одинокой<br>В тумане моря голубом!..</pre>" }
     * @example { "id": "<pre>a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11</pre>" }
     * @example { "id": "'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'" }
     */
    public function testFind(UnitTester $I, Example $example): void
    {
        $id = $example['id'];

        try {
            $this->_repository->find($id);
        } catch (Throwable $throwable) {
            $I->assertEquals(DatabaseException::class, get_class($throwable));

            return;
        }

        $I->assertTrue(false);
    }

    /**
     * Тестирование метода findOneBy
     *
     * @param UnitTester $I
     * @param Example $example
     *
     * @example { "id": "guid" }
     * @example { "id": "1" }
     * @example { "id": 1 }
     * @example { "id": "_)(*&^%$#@" }
     * @example { "id": "<?php echo 'guid';>" }
     * @example { "id": "null" }
     * @example { "id": 88005553535 }
     * @example { "id": "8-800-555-35-35" }
     * @example { "id": "8(800)555-35-35" }
     * @example { "id": "    " }
     * @example { "id": "" }
     * @example { "id": "[]" }
     * @example { "id": "<pre>Белеет парус одинокой<br>В тумане моря голубом!..</pre>" }
     * @example { "id": "<pre>a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11</pre>" }
     * @example { "id": "'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'" }
     */
    public function testFindOneBy(UnitTester $I, Example $example): void
    {
        $criteria = $example['id'];

        try {
            $this->_repository->findOneBy(['guid' => $criteria]);
        } catch (Throwable $throwable) {
            $I->assertEquals(DatabaseException::class, get_class($throwable));

            return;
        }

        $I->assertTrue(false);
    }

    /**
     * Тестирование метода findBy
     *
     * @param UnitTester $I
     * @param Example $example
     *
     * @example { "id": "trail" }
     * @example { "id": "1" }
     * @example { "id": 1 }
     * @example { "id": "_)(*&^%$#@" }
     * @example { "id": "<?php echo 'guid';>" }
     * @example { "id": "null" }
     * @example { "id": 88005553535 }
     * @example { "id": "8-800-555-35-35" }
     * @example { "id": "8(800)555-35-35" }
     * @example { "id": "    " }
     * @example { "id": "" }
     * @example { "id": "[]" }
     * @example { "id": "<pre>Белеет парус одинокой<br>В тумане моря голубом!..</pre>" }
     * @example { "id": "<pre>a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11</pre>" }
     * @example { "id": "'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'" }
     */
    public function testFindBy(UnitTester $I, Example $example): void
    {
        $criteria = $example['id'];

        try {
            $this->_repository->findBy(['guid' => $criteria]);
        } catch (Throwable $throwable) {
            $I->assertEquals(DatabaseException::class, get_class($throwable));

            return;
        }

        $I->assertTrue(false);
    }

    /**
     * Тестирование метода count
     *
     * @param UnitTester $I
     * @param Example $example
     *
     * @example { "id": "guid" }
     * @example { "id": "1" }
     * @example { "id": 1 }
     * @example { "id": "_)(*&^%$#@" }
     * @example { "id": "<?php echo 'guid';>" }
     * @example { "id": "null" }
     * @example { "id": 88005553535 }
     * @example { "id": "8-800-555-35-35" }
     * @example { "id": "8(800)555-35-35" }
     * @example { "id": "    " }
     * @example { "id": "" }
     * @example { "id": "[]" }
     * @example { "id": "<pre>Белеет парус одинокой<br>В тумане моря голубом!..</pre>" }
     * @example { "id": "<pre>a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11</pre>" }
     * @example { "id": "'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'" }
     */
    public function testCount(UnitTester $I, Example $example): void
    {
        $criteria = $example['id'];

        try {
            $this->_repository->count(['guid' => $criteria]);
        } catch (Throwable $throwable) {
            $I->assertEquals(DatabaseException::class, get_class($throwable));

            return;
        }

        $I->assertTrue(false);
    }
}
