<?php
declare(strict_types=1);

namespace App\Tests\unit;

use App\Kernel;
use App\Tests\UnitTester;
use Codeception\Exception\ConfigurationException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Базовый класс для unit тестов
 *
 * @package App\Tests
 */
abstract class AbstractUnitCest
{
    /** @var ContainerInterface */
    protected $_container;

    /**
     * @inheritDoc
     * @param UnitTester $I
     * @throws ConfigurationException
     */
    public function _before(UnitTester $I): void
    {
        /** @var Kernel $kernel */
        $kernel = $I->grabService('kernel');

        $kernel->boot();

        $container = $kernel->getContainer();

        if ($container === null) {
            throw new ConfigurationException('Контейнер не установлен');
        }

        $this->_container = $container;
    }
}
