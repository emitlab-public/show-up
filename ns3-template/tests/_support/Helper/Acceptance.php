<?php

namespace App\Tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use App\Tests\AcceptanceTester;
use Codeception\Exception\ModuleException;
use Codeception\Module;
use Codeception\Module\Db;
use Codeception\Util\Fixtures;

/**
 * TODO: разобраться в будущем и возможно перенести сюда методы из AbstractCest
 *
 * @package App\Tests\Helper
 */
class Acceptance extends Module
{
    /**
     * @param string $table Имя таблицы из которой удаляем
     * @param array $criteria Параметры выборки для удаления
     * Если пусто, значит очищаем всю таблицу
     * @return bool
     * @throws ModuleException
     */
    public function deleteFromDatabase(string $table, array $criteria = []): bool
    {
        /** @var Db $dbModule */
        $dbModule = $this->getModule('Db');

        $pdo = $dbModule->_getDbh();

        $query = 'delete from %s where %s';

        $isDropAll = empty($criteria);

        if ($isDropAll) {
            $query = sprintf($query, $table, '1 = 1');
        } else {
            $params = [];

            foreach ($criteria as $key => $value) {
                $params[] = "$key = ?";
            }

            $query = sprintf($query, $table, implode(' AND ', $params));

            $this->debugSection(__FUNCTION__, $query);
        }

        return $pdo->prepare($query)
            ->execute($isDropAll ? null : array_values($criteria));
    }
}
