<?php
declare(strict_types=1);

return [
    [
        'guid' => '0dcc5170-690a-43c8-9277-5b6d96e270ff',
        'created' => '2019-05-08 10:49:33',
        'updated' => '2019-05-08 10:49:33',
        'type' => 'sms',
        'name' => 'test_tmpl_1',
        'message' => 'Some Message'
    ],
    [
        'guid' => '0dcc5170-690a-43c8-9277-5b6d96e270f3',
        'created' => '2019-05-08 10:49:33',
        'updated' => '2019-05-08 10:49:33',
        'type' => 'push',
        'name' => 'test_tmpl_2',
        'message' => 'Some Message2'
    ]
];
