<?php
declare(strict_types=1);

return [
    [
        'guid' => '2860daaa-f777-4a3f-940e-de66091771d3',
        'created' => '2019-05-08 08:00:06',
        'updated' => '2019-05-08 08:00:06',
        'perm_name' => 'GET_TEMPLATES_LIST',
        'role_name' => 'ADMIN'
    ],
    [
        'guid' => '6a7cd65c-9744-445d-96e5-8a2e2fb6fc6a',
        'created' => '2019-05-08 08:00:06',
        'updated' => '2019-05-08 08:00:06',
        'perm_name' => 'VIEW_TEMPLATE',
        'role_name' => 'ADMIN'
    ],
    [
        'guid' => '56941502-7238-47b7-860a-879803aa8347',
        'created' => '2019-05-08 08:00:06',
        'updated' => '2019-05-08 08:00:06',
        'perm_name' => 'CREATE_TEMPLATE',
        'role_name' => 'ADMIN',
    ],
    [
        'guid' => 'b965ae27-4e16-4e2a-8d8a-35ae3e8309f2',
        'created' => '2019-05-08 08:00:06',
        'updated' => '2019-05-08 08:00:06',
        'perm_name' => 'UPDATE_TEMPLATE',
        'role_name' => 'ADMIN',
    ],
    [
        'guid' => '8a9f7428-5929-42d1-8a20-240438b23fdc',
        'created' => '2019-05-08 08:00:06',
        'updated' => '2019-05-08 08:00:06',
        'perm_name' => 'DELETE_TEMPLATE',
        'role_name' => 'ADMIN',
    ],
];
