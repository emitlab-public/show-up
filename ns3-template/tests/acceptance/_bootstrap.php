<?php
declare(strict_types=1);

use Codeception\Util\Fixtures;

define('FIXTURES_DIR', __DIR__ . '/../_fixtures');

Fixtures::add('perms', require FIXTURES_DIR . '/perms.php');
Fixtures::add('templates', require FIXTURES_DIR . '/templates.php');
