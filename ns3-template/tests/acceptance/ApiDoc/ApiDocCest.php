<?php
declare(strict_types=1);

namespace App\Tests\acceptance\ApiDoc;

use App\Tests\{AbstractCest, AcceptanceTester};

/**
 * Тестирование apiDoc
 *
 * @package App\Tests\acceptance\ApiDoc
 */
class ApiDocCest extends AbstractCest
{
    /**
     * Проверка что ApiDoc не доступен
     *
     * @param AcceptanceTester $I
     */
    public function dontSeeApiDoc(AcceptanceTester $I): void
    {
        $I->sendGET('/api/doc');
        $this->checkFail($I);

        $response = json_decode($I->grabResponse(), true);

        $I->assertIsString($response['error']);
        $I->assertEquals('No route found for "GET /api/doc"', $response['error']);
    }
}
