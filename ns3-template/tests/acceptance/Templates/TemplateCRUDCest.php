<?php
declare(strict_types=1);

namespace App\Tests\acceptance\Templates;

use App\Tests\{AbstractCest, AcceptanceTester};
use App\Entity\Template;
use Codeception\Example;
use Codeception\Util\HttpCode;

/**
 * Тестирование данных для списка и просмотра шаблонов
 *
 * @package App\Tests
 */
class TemplateCRUDCest extends AbstractCest
{
    /**
     * @inheritDoc
     * @param AcceptanceTester $I
     */
    public function _before(AcceptanceTester $I): void
    {
        $this->applyFixture($I, 'perms');
        $this->applyFixture($I, 'templates');
    }

    /**
     * @inheritDoc
     * @param AcceptanceTester $I
     */
    public function _after(AcceptanceTester $I): void
    {
        $I->deleteFromDatabase('templates');
    }

    /**
     * Тестирование метода GET v1/templates
     * Получение списка с пагинацией
     *
     * @param AcceptanceTester $I
     */
    public function index(AcceptanceTester $I): void
    {
        $I->sendGET('/v1/templates');
        $this->checkSuccess($I);

        $data = $this->getResponseData($I);

        // Проверка структуры пагинатора
        $I->assertTrue(array_key_exists('items', $data));
        $I->assertTrue(array_key_exists('page', $data));
        $I->assertTrue(array_key_exists('pages', $data));
        $I->assertTrue(array_key_exists('pageSize', $data));
        $I->assertTrue(array_key_exists('totalCount', $data));
    }

    /**
     * Тестирование метода GET v1/templates
     * Получение списка с пагинацией и указание кол-ва документов на странице
     *
     * @param AcceptanceTester $I
     */
    public function perPage(AcceptanceTester $I): void
    {
        $I->sendGET('/v1/templates?per-page=1');
        $this->checkSuccess($I);

        $data = $this->getResponseData($I);

        // Проверим что кол-во страниц совпадает с количеством данных
        $I->assertEquals($data['pages'], $data['totalCount']);
    }

    /**
     * Тестирование метода GET v1/templates/{guid}
     * Получение 1й записи
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "guid": "0dcc5170-690a-43c8-9277-5b6d96e270ff" }
     * @example { "guid": "0dcc5170-690a-43c8-9277-5b6d96e270f3" }
     */
    public function view(AcceptanceTester $I, Example $example): void
    {
        $guid = $example['guid'];

        $I->sendGET("/v1/templates/$guid");
        $this->checkSuccess($I);

        $data = $this->getResponseData($I);

        /** @var Template $template */
        $template = $I->grabEntityFromRepository(Template::class, [
            'guid' => $guid
        ]);

        foreach ($data as $key => $value) {
            $I->assertSame($value, $template->{$key});
        }
    }

    /**
     * Тестирование метода GET v1/templates/{guid}
     * Получение 1й записи
     * Провальный тест
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "guid": "some_guid" }
     * @example { "guid": "1" }
     * @example { "guid": null }
     * @example { "guid": "0dcc5170-1111-1111-1111-5b6d96e270f3" }
     * @example { "guid": "" }
     * @example { "guid": "/*-" }
     * @example { "guid": 13 }
     * @example { "guid": "русский" }
     */
    public function failView(AcceptanceTester $I, Example $example): void
    {
        $guid = $example['guid'];

        $I->sendGET("/v1/templates/$guid");
        $I->cantSeeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }

    /**
     * Тестирование метода POST v1/templates
     * Создание 1й записи
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "type": "sms", "name": "test_sms", "message": "Test Sms Message" }
     * @example { "type": "push", "name": "test_push", "message": "Test Push Message" }
     * @example { "type": "email", "name": "test_email", "message": "Test EMail Message" }
     */
    public function create(AcceptanceTester $I, Example $example): void
    {
        $I->sendPOST('/v1/templates', [
            'type' => $example['type'],
            'name' => $example['name'],
            'message' => $example['message']
        ]);

        $this->checkSuccess($I);

        $data = $this->getResponseData($I);

        $fields = ['type', 'name', 'message'];

        foreach ($fields as $fieldName) {
            $I->assertSame($example[$fieldName], $data[$fieldName]);
        }
    }

    /**
     * Тестирование метода POST v1/templates
     * Провальный тест
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "type": "", "name": "", "message": "" }
     * @example { "type": "not_exist", "name": "", "message": "" }
     * @example { "type": "_sms", "name": "", "message": "" }
     * @example { "type": "email", "name": "", "message": "" }
     */
    public function failCreate(AcceptanceTester $I, Example $example): void
    {
        $I->sendPOST('/v1/templates', [
            'type' => $example['type'],
            'name' => $example['name'],
            'message' => $example['message']
        ]);
        $I->cantSeeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }

    /**
     * Тестирование метода PUT v1/templates/{guid}
     * Обновление 1й записи
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "guid": "0dcc5170-690a-43c8-9277-5b6d96e270ff", "data": { "message": "message edited" } }
     * @example { "guid": "0dcc5170-690a-43c8-9277-5b6d96e270ff", "data": { "message": "message edited v2", "type": "email" } }
     */
    public function update(AcceptanceTester $I, Example $example): void
    {
        $data = $example['data'];

        $I->sendPUT("/v1/templates/${example['guid']}", $data);

        $this->checkSuccess($I);

        $response = $this->getResponseData($I);

        foreach ($data as $key => $value) {
            $I->assertSame($value, $response[$key]);
        }
    }

    /**
     * Тестирование метода PUT v1/templates/{guid}
     * Провальный тест
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "guid": "0dcc5170-690a-43c8-9277-5b6d96e270ff", "data": { "message": "message edited v2", "type": "not_exist_type" } }
     */
    public function failUpdate(AcceptanceTester $I, Example $example): void
    {
        $data = $example['data'];

        $I->sendPUT("/v1/templates/${example['guid']}", $data);

        $this->checkFail($I);
    }

    /**
     * Тестирование метода DELETE v1/templates/{guid}
     * Удаление 1й записи
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "guid": "0dcc5170-690a-43c8-9277-5b6d96e270ff" }
     */
    public function delete(AcceptanceTester $I, Example $example): void
    {
        $I->sendDELETE("/v1/templates/${example['guid']}");

        $this->checkSuccess($I);

        $content = json_decode($I->grabResponse(), true);

        $I->assertTrue($content['data']);
        $I->cantSeeInDatabase('templates', ['guid' => $example['guid']]);
    }

    /**
     * Тестирование метода DELETE v1/templates/{guid}
     * Провальный тест
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "guid": "some_guid" }
     * @example { "guid": "1" }
     * @example { "guid": null }
     * @example { "guid": "0dcc5170-1111-1111-1111-5b6d96e270f3" }
     */
    public function failDelete(AcceptanceTester $I, Example $example): void
    {
        $I->sendDELETE("/v1/templates/${example['guid']}");

        $this->checkFail($I);
    }

    /**
     * Попытка постучаться к методам, которые защищены правом
     * Очистим таблицу прав и будем стучаться с токеном
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "url": "/v1/templates", "method": "sendGET" }
     * @example { "url": "/v1/templates/0dcc5170-690a-43c8-9277-5b6d96e270ff", "method": "sendGET" }
     * @example { "url": "/v1/templates", "method": "sendPOST" }
     * @example { "url": "/v1/templates/0dcc5170-690a-43c8-9277-5b6d96e270ff", "method": "sendPUT" }
     * @example { "url": "/v1/templates/0dcc5170-690a-43c8-9277-5b6d96e270ff", "method": "sendDELETE" }
     */
    public function perms(AcceptanceTester $I, Example $example): void
    {
        $I->deleteFromDatabase('perms');

        $method = $example['method'];
        $url = $example['url'];

        $I->{$method}($url);

        $this->checkFail($I);

        $response = $this->getResponseData($I);

        $I->assertEquals('Access denied', $response['message']);
        $I->assertEquals(400, $response['code']);
        $I->assertEquals('Gp\\Bundle\\Perms\\Exception\\AccessDeniedException', $response['type']);
    }

    /**
     * Попытка постучаться к методам, которые защищены правом
     * Стучимся без токена
     *
     * @param AcceptanceTester $I
     * @param Example $example
     *
     * @example { "url": "/v1/templates", "method": "sendGET" }
     * @example { "url": "/v1/templates/0dcc5170-690a-43c8-9277-5b6d96e270ff", "method": "sendGET" }
     * @example { "url": "/v1/templates", "method": "sendPOST" }
     * @example { "url": "/v1/templates/0dcc5170-690a-43c8-9277-5b6d96e270ff", "method": "sendPUT" }
     * @example { "url": "/v1/templates/0dcc5170-690a-43c8-9277-5b6d96e270ff", "method": "sendDELETE" }
     */
    public function permsToken(AcceptanceTester $I, Example $example): void
    {
        $I->deleteHeader('X-Service-Token');

        $method = $example['method'];
        $url = $example['url'];

        $I->{$method}($url);

        $this->checkFail($I);

        $response = $this->getResponseData($I);

        $I->assertEquals('Access denied', $response['message']);
        $I->assertEquals(400, $response['code']);
        $I->assertEquals('Gp\\Bundle\\Perms\\Exception\\AccessDeniedException', $response['type']);
    }
}
