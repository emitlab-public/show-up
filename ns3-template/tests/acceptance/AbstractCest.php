<?php
declare(strict_types=1);

namespace App\Tests;

use Codeception\Util\{Fixtures, HttpCode};

/**
 * Базовый класс для тестов
 *
 * @package App\Tests
 */
abstract class AbstractCest
{
    /**
     * Применение фикстур
     *
     * @param AcceptanceTester $I Экземпляр тестера
     * @param string $name Имя фиксутры
     * @return int
     */
    protected function applyFixture(AcceptanceTester $I, string $name): int
    {
        $result = 0;

        if (Fixtures::exists($name)) {
            foreach (Fixtures::get($name) as $data) {
                $result += $I->haveInDatabase($name, $data);
            }
        }

        return $result;
    }

    /**
     * Получение объекта data из ответа
     *
     * @param AcceptanceTester $I
     * @return array
     */
    protected function getResponseData(AcceptanceTester $I): array
    {
        $content = json_decode($I->grabResponse(), true);

        $data = $content['data'] ?? [];

        $I->assertNotEmpty($data);

        return $data;
    }

    /**
     * Проверка что запрос прошел успешно
     *
     * @param AcceptanceTester $I
     */
    protected function checkSuccess(AcceptanceTester $I): void
    {
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $content = json_decode($I->grabResponse(), true);

        $I->assertTrue($content['success']);
        $I->assertSame(0, $content['errno']);
        $I->assertSame('', $content['error']);

        $I->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'errno' => 'integer',
            'error' => 'string|null',
            'data' => 'array|string|boolean'
        ]);
    }

    /**
     * Проверка что запрос не прошел успешно
     *
     * @param AcceptanceTester $I
     */
    protected function checkFail(AcceptanceTester $I): void
    {
        $I->cantSeeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $content = json_decode($I->grabResponse(), true);

        $I->assertFalse($content['success']);
        $I->assertNotSame(0, $content['errno']);
        $I->assertNotSame('', $content['error']);

        $I->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'errno' => 'integer',
            'error' => 'string|null',
            'data' => 'array|string|boolean'
        ]);
    }
}
