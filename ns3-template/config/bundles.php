<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    Nelmio\ApiDocBundle\NelmioApiDocBundle::class => ['dev' => true],
    Gp\Bundle\Perms\GpPermsBundle::class => ['all' => true],
    Gp\Bundle\Response\GpResponseBundle::class => ['all' => true],
    Gp\Bundle\Validation\GpValidationBundle::class => ['all' => true],
    Gp\Bundle\EventBus\GpEventBusBundle::class => ['all' => true],
    Gp\Bundle\Amqp\GpAmqpBundle::class => ['all' => true],
];
