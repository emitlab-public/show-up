<?php
declare(strict_types=1);

namespace App\Entity;

use Gp\Component\EntityHelper\EntityLifeCyclesTrait;
use Swagger\Annotations as SWG;
use DateTime;

/**
 * Сущность для работы с шаблонами
 *
 * @package App\Entity
 */
class Template
{
    use EntityLifeCyclesTrait;

    /**
     * @var string Идентификатор записи
     * @SWG\Property(type="string", description="Идентификатор записи")
     */
    public $guid;

    /**
     * @var DateTime|string
     * @SWG\Property(type="string", description="Дата/Время создания записи")
     */
    public $created;

    /**
     * @var DateTime|string
     * @SWG\Property(type="string", description="Дата/Время последней модификации записи")
     */
    public $updated;

    /**
     * @var string
     * @SWG\Property(type="string", description="Тип шаблона")
     */
    public $type;

    /**
     * @var string
     * @SWG\Property(type="string", description="Имя шаблона")
     */
    public $name;

    /**
     * @var string
     * @SWG\Property(type="string", description="Текст шаблона")
     */
    public $message;
}
