<?php
declare(strict_types=1);

namespace App\Repository;

use App\Exception\DatabaseException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Gp\Component\Pagination\Pagination;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * Базовый репозиторий
 *
 * @package App\Repository
 */
abstract class AbstractRepository extends ServiceEntityRepository
{
    /** @var LoggerInterface */
    protected $_logger;

    /** @var TranslatorInterface */
    private $_translator;

    /**
     * AbstractRepository constructor.
     * @param RegistryInterface $registry
     * @param string $entity
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     */
    public function __construct(
        RegistryInterface $registry,
        string $entity,
        LoggerInterface $logger,
        TranslatorInterface $translator
    )
    {
        $this->_logger = $logger;
        $this->_translator = $translator;

        parent::__construct($registry, $entity);
    }

    /**
     * Сохранение данных в модели
     *
     * @param object $entity Сущность
     * @param bool $flush Применить ли изменения
     * @throws DatabaseException
     */
    public function save($entity, bool $flush = true): void
    {
        try {
            // Говорим Doctrine что необходимо подготовить запрос (INSERT | UPDATE)
            $this->_em->persist($entity);

            if ($flush) {
                // Выполняем запрос (запросы)
                $this->_em->flush();
            }
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }
    }

    /**
     * Удаление модели
     *
     * @param object $entity Сущность
     * @param bool $flush Применить ли изменения
     * @throws DatabaseException
     */
    public function remove($entity, bool $flush = true): void
    {
        try {
            // Говорим Doctrine что необходимо подготовить запрос DELETE
            $this->_em->remove($entity);

            if ($flush) {
                // Выполняем запрос (запросы)
                $this->_em->flush();
            }
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }
    }

    /**
     * Получение пагинированного списка
     *
     * @param Pagination $pager Пагинатор
     * @param array $condition Дополнительное условие выборки
     * @return array
     * @throws DatabaseException
     */
    public function getList(Pagination $pager, array $condition = []): array
    {
        foreach ($pager->getFilter() as $key => $value) {
            if ($this->fieldExist($key)) {
                $condition[$key] = $value;
            }
        }

        $orderBy = [];
        foreach ($pager->getSort() as $key => $value) {
            if ($this->fieldExist($key)) {
                $orderBy[$key] = $value === SORT_ASC ? 'ASC' : 'DESC';
            }
        }

        return [
            $this->count($condition),
            $this->findBy($condition, $orderBy, $pager->getPerPage(), $pager->getOffset())
        ];
    }

    /**
     * Получение имен полей сущности
     * Возвращает так же имена полей отношений (manyToOne, etc...)
     *
     * @return array
     */
    public function getFieldNames(): array
    {
        return array_keys($this->_class->reflFields);
    }

    /**
     * Проверяет существует ли поле в классе / таблице
     *
     * @param string $name Имя поля
     * @return bool
     */
    protected function fieldExist(string $name): bool
    {
        return isset($this->_class->fieldNames[$name]);
    }

    /**
     * Применяет все изменения
     *
     * @throws DatabaseException
     */
    public function flush(): void
    {
        try {
            $this->_em->flush();
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }
    }

    /**
     * @inheritDoc
     * @return array
     * @throws DatabaseException
     */
    public function findAll(): array
    {
        try {
            return parent::findAll();
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }

        return [];
    }

    /**
     * @inheritDoc
     * @param array $criteria
     * @param array|null $orderBy
     * @return object|null
     * @throws DatabaseException
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        try {
            return parent::findOneBy($criteria, $orderBy);
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }

        return null;
    }

    /**
     * @inheritDoc
     * @param mixed $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return object|null
     * @throws DatabaseException
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        try {
            return parent::find($id, $lockMode, $lockVersion);
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }

        return null;
    }

    /**
     * @inheritDoc
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array
     * @throws DatabaseException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        try {
            return parent::findBy($criteria, $orderBy, $limit, $offset);
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }

        return [];
    }

    /**
     * @inheritDoc
     * @param array $criteria
     * @return int
     * @throws DatabaseException
     */
    public function count(array $criteria): int
    {
        try {
            return parent::count($criteria);
        } catch (Throwable $e) {
            $this->logAndThrow(__FUNCTION__, $e);
        }

        return 0;
    }

    /**
     * Логируем исключение и бросаем общую ошибку БД пользователю
     *
     * @param string $methodName Имя метода где произшло исключение
     * @param Throwable $e Экземпляр исключения
     * @throws DatabaseException
     */
    private function logAndThrow(string $methodName, Throwable $e): void
    {
        $this->_logger->error('Database error while ' . $methodName, [
            'sqlstate' => strstr($e->getMessage(), 'SQLSTATE') ?? 'Unknown sql error',
            'message' => $e->getMessage(),
            'trace' => $e->getTrace(),
            'method' => $methodName,
        ]);

        throw new DatabaseException(
            $this->_translator->trans('exceptions.database')
        );
    }
}
