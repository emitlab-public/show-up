<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Template;
use App\Exception\DatabaseException;
use Gp\Component\EntityHelper\EntityTrait;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @method Template|null find($id, $lockMode = null, $lockVersion = null)
 * @method Template|null findOneBy(array $criteria, array $orderBy = null)
 * @method Template[]    findAll()
 * @method Template[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemplateRepository extends AbstractRepository
{
    use EntityTrait;

    /**
     * TemplateRepositoryRepository constructor.
     * @param RegistryInterface $registry
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     */
    public function __construct(RegistryInterface $registry, LoggerInterface $logger, TranslatorInterface $translator)
    {
        parent::__construct($registry, Template::class, $logger, $translator);
    }

    /**
     * Создание шаблона
     *
     * @param array $data Информация для создания
     * @return Template
     * @throws DatabaseException
     */
    public function create(array $data): Template
    {
        $template = new Template();
        $this->load($template, $data);

        $this->save($template);

        return $template;
    }

    /**
     * Обновление шаблона
     *
     * @param Template $template Обновляемый шаблон
     * @param array $data Обновляемая информация
     * @return Template
     * @throws DatabaseException
     */
    public function update(Template $template, array $data): Template
    {
        $this->load($template, $data);

        $this->save($template);

        return $template;
    }
}
