<?php
declare(strict_types=1);

namespace App\Sender;

use Gp\Bundle\EventBus\Sender\BasicSender;

/**
 * Сервис для поддержки старго способа отправки межсервисных событий/сообщений
 *
 * TODO: удалить при окончательном переходе на новый формат очередей
 * @deprecated
 * @package App\Service
 */
class LegacyEventSender extends BasicSender
{
    /**
     * @inheritDoc
     */
    public function registeredEvents(): array
    {
        return [
            'sender_transport::sms.*' => 'Событие обработки шаблона для sms сообщения.',
            'sender_transport::push.*' => 'Событие обработки шаблона для пуш-уведомлений.',
            'sender_transport::email.*' => 'Событие обработки шаблона для электронного письма.'
        ];
    }
}
