<?php
declare(strict_types=1);

namespace App\Sender;

use Gp\Bundle\EventBus\Sender\BasicSender;

/**
 * Сервис отправки межсервисных сообщений
 *
 * @package App\Sender
 */
class ExternalEventSender extends BasicSender
{
    /**
     * @inheritDoc
     */
    public function registeredEvents(): array
    {
        return [
            'v1.sms.resolve.*' => 'Событие обработки шаблона для sms сообщения.',
            'v1.push.resolve.*' => 'Событие обработки шаблона для пуш-уведомлений.',
            'v1.email.resolve.*' => 'Событие обработки шаблона для электронного письма.'
        ];
    }
}
