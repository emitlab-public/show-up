<?php
declare(strict_types=1);

namespace App\Exception;

use Gp\Tools\Exception\AbstractException;

/**
 * Исключение при неверном сообщении AMQP
 *
 * @package App\Exceptions
 */
class AMQPMessageException extends AbstractException
{
    public $statusCode = 404;
    public $defaultCode = 1001;

    /**
     * Получение сообщения исключения
     *
     * @return string
     */
    public function getDefaultMessage(): string
    {
        return 'Missing required param in AMQP message';
    }
}
