<?php
declare(strict_types=1);

namespace App\Exception;

use Gp\Tools\Exception\AbstractException;

/**
 * Ошибка когда не найдена модель
 *
 * @package App\Exceptions
 */
class ModelNotFoundException extends AbstractException
{
    public $statusCode = 404;
    public $defaultCode = 1004;

    /**
     * Получение сообщения исключения
     *
     * @return string
     */
    public function getDefaultMessage(): string
    {
        return 'Model not found';
    }
}
