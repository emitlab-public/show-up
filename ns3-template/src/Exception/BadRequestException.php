<?php
declare(strict_types=1);

namespace App\Exception;

use Gp\Tools\Exception\AbstractException;

/**
 * Исключение кода неверные данные запроса, либо валидация
 *
 * @package App\Exceptions
 */
class BadRequestException extends AbstractException
{
    public $statusCode = 403;
    public $defaultCode = 1003;

    /**
     * Получение сообщения исключения
     *
     * @return string
     */
    public function getDefaultMessage(): string
    {
        return 'Bad request';
    }
}
