<?php
declare(strict_types=1);

namespace App\Exception;

use Gp\Tools\Exception\AbstractException;

/**
 * Исключение при работе с базой данных
 *
 * @package App\Exception
 */
class DatabaseException extends AbstractException
{
    public $defaultCode = 1005;

    /**
     * @inheritDoc
     * @return string
     */
    public function getDefaultMessage(): string
    {
        return 'Database exception';
    }
}
