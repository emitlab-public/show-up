<?php
declare(strict_types=1);

namespace App\Exception;

use Gp\Tools\Exception\AbstractException;

/**
 * Неизвестное поле класса
 *
 * @package App\Exceptions
 */
class UnknownPropertyException extends AbstractException
{
    public $statusCode = 403;
    public $defaultCode = 1002;

    /**
     * Получение сообщения исключения
     *
     * @return string
     */
    public function getDefaultMessage(): string
    {
        return 'Unknown property';
    }
}
