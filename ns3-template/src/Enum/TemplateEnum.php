<?php
declare(strict_types=1);

namespace App\Enum;

use Gp\Bundle\Validation\Constraints\ExistInTable\EnumTypeInterface;

/**
 * Перечисление значений для поля type сущности Template
 *
 * @package App\Enum
 */
class TemplateEnum implements EnumTypeInterface
{
    // Тип email сообщения
    public const TYPE_EMAIL = 'email';

    // Тип смс сообщения
    public const TYPE_SMS = 'sms';

    // Тип push уведомлений
    public const TYPE_PUSH = 'push';

    /**
     * @inheritDoc
     * @return array
     */
    public function getValues(): array
    {
        return [
            self::TYPE_PUSH,
            self::TYPE_SMS,
            self::TYPE_EMAIL,
        ];
    }
}
