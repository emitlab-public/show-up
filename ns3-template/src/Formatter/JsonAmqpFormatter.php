<?php
declare(strict_types=1);

namespace App\Formatter;

use Monolog\Formatter\JsonFormatter;

/**
 * Форматтер для AMQP сообщений в Monolog
 *
 * TODO: кандидат в Tools
 *
 * @package App\Formatter
 */
class JsonAmqpFormatter extends JsonFormatter
{
    /**
     * @inheritDoc
     * @param array $record
     * @return array|mixed|string
     */
    public function format(array $record)
    {
        $record['context']['amqp']['message']->body = json_decode($record['context']['amqp']['message']->body, true);

        return parent::format($record);
    }
}
