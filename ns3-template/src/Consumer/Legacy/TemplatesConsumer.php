<?php
declare(strict_types=1);

namespace App\Consumer\Legacy;

use App\Exception\ModelNotFoundException;
use App\Sender\LegacyEventSender;
use App\Service\TemplateService;
use Gp\Bundle\EventBus\Consumer\BasicConsumer;
use Gp\Bundle\EventBus\Exception\SenderException;
use Gp\Bundle\EventBus\Interfaces\EventBusSubscriberInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Error\{LoaderError, RuntimeError, SyntaxError};

/**
 * Консьюмер, обрабатывающий шаблоны в старом формате очередей
 *
 * @package App\Consumer\Legacy
 */
class TemplatesConsumer extends BasicConsumer
{
    /**
     * @var LegacyEventSender
     */
    private $_eventSender;

    /**
     * @var TemplateService
     */
    private $_templateService;

    /**
     * TemplatesConsumer constructor.
     *
     * @param EventBusSubscriberInterface $sbr
     * @param ContainerInterface $container
     * @param LoggerInterface $logger
     * @param LegacyEventSender $eventSender
     * @param TemplateService $templateService
     */
    public function __construct(
        EventBusSubscriberInterface $sbr,
        ContainerInterface $container,
        LoggerInterface $logger,
        LegacyEventSender $eventSender,
        TemplateService $templateService
    )
    {
        $this->_eventSender = $eventSender;
        $this->_templateService = $templateService;

        parent::__construct($sbr, $container, $logger);
    }

    /**
     * @inheritDoc
     */
    public function registerHandlers(): array
    {
        return [
            // Универсальное событие ключа маршрутизации нет
            'sender_templates::#' => [
                ['resolveHandler', 'workerCount' => 3]
            ]
        ];
    }

    /**
     * Обрабатывает шаблоны, полученные от других микросервисов
     *
     * @param array $message
     * @return bool
     * @throws ModelNotFoundException
     * @throws SenderException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function resolveHandler(array $message): bool
    {
        $paramNames = ['guid', 'type', 'templateName', 'templateParams'];

        [$guid, $type, $name, $params] = $this->getRequiredFields($paramNames, $message);

        $this->_logger->info(
            sprintf('Message %s rendering', $guid)
        );

        $renderedMessage = $this->_templateService->render($type, $name, $params);

        $this->_logger->info(
            sprintf('Message %s rendered. Body: %s', $guid, $renderedMessage)
        );

        $message['renderedMessage'] = $renderedMessage;

        $event = sprintf('%s.%s', $type, $name);

        $this->_eventSender->send($event, $message);

        $this->_logger->info(
            sprintf('Message %s pushed to sender', $event)
        );

        return true;
    }
}
