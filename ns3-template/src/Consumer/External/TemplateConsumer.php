<?php
declare(strict_types=1);

namespace App\Consumer\External;

use Gp\Bundle\EventBus\Consumer\BasicConsumer;

/**
 * Консьюмер, обрабатывающий шаблоны
 *
 * @package App\Consumer\External
 */
class TemplateConsumer extends BasicConsumer
{

    /**
     * @inheritDoc
     */
    public function registerHandlers(): array
    {
        return [
            'realtors::v1.template.send' => [
                ['resolveHandler']
            ]
        ];
    }

    /**
     * @param array $message
     * @return bool
     */
    public function resolveHandler(array $message): bool
    {
        $this->_logger->debug(
            $this->_serviceName . ' - get message in resolve handler. ' . print_r($message, true)
        );

        return true;
    }
}
