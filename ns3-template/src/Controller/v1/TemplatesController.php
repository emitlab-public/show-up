<?php
declare(strict_types=1);

namespace App\Controller\v1;

use App\Enum\TemplateEnum;
use App\Exception\{BadRequestException, DatabaseException, ModelNotFoundException};
use App\Service\TemplateService;
use Doctrine\ORM\NonUniqueResultException;
use Gp\Bundle\Perms\Interfaces\AuthInterface;
use Gp\Bundle\Response\Controller\RequestTrait;
use Gp\Bundle\Validation\Constraints\Enum\Enum;
use Gp\Component\Pagination\Controller\PaginationTrait;
use Gp\Tools\Service\ValidationService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Annotations as SWG;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Контроллер для работы с шаблонами
 *
 * @package App\Controller
 */
class TemplatesController extends AbstractController implements AuthInterface
{
    use RequestTrait;
    use PaginationTrait;

    /** @var TemplateService */
    private $_templateService;

    /** @var ValidationService */
    private $_validationService;

    /** @var TranslatorInterface */
    private $_translator;

    /**
     * TemplatesController constructor.
     * @param TemplateService $templateService
     * @param ValidationService $validationService
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TemplateService $templateService,
        ValidationService $validationService,
        TranslatorInterface $translator
    )
    {
        $this->_templateService = $templateService;
        $this->_validationService = $validationService;
        $this->_translator = $translator;
    }

    /**
     * Список шаблонов
     *
     * @SWG\Get(
     *     tags={"templates"},
     *     @SWG\Response(
     *          response="200",
     *          description="Список шаблонов",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object",
     *                  @SWG\Property(property="items", type="array", @SWG\Items(ref=@Model(type=App\Entity\Template::class))),
     *                  @SWG\Property(property="page", type="integer", example=1),
     *                  @SWG\Property(property="pages", type="integer", example=1),
     *                  @SWG\Property(property="pageSize", type="integer", example=20),
     *                  @SWG\Property(property="totalCount", type="integer", example=20),
     *              ),
     *          ),
     *     ),
     *     @SWG\Parameter(
     *          type="string",
     *          in="header",
     *          name="X-User-Token",
     *          description="Токен пользователя",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *     ),
     *     @SWG\Parameter(type="integer", in="query", name="page", description="Номер страницы", default="1"),
     *     @SWG\Parameter(type="integer", in="query", name="per-page", description="Количество данных на странице", default="20"),
     *     @SWG\Parameter(type="string", in="query", name="sort", description="Сортировка", format="-?fielname,field2"),
     * )
     *
     * Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355'
     *          'http://localhost/v1/templates'
     *
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function index(): JsonResponse
    {
        return $this->json(
            $this->_templateService
                ->getList($this->getPager())
                ->getPageableData()
        );
    }

    /**
     * Создает новый шаблон с указанными параметрами
     *
     * @SWG\Post(
     *      tags={"templates"},
     *      @SWG\Response(
     *          response="200",
     *          description="Информация о созданном шаблоне",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref=@Model(type=App\Entity\Template::class)),
     *          ),
     *      ),
     *      @SWG\Response(
     *          response="403",
     *          description="Ошибка валидации",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", example="false"),
     *              @SWG\Property(property="errno", type="integer", example="403"),
     *              @SWG\Property(property="error", type="string", example="Ошибка валидации", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref="#/definitions/ErrorData"),
     *          ),
     *      ),
     *      @SWG\Response(
     *          response="400",
     *          description="Ошибка создания",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="Ошибка базы данных", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref="#/definitions/ErrorData"),
     *          ),
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="header",
     *          name="X-User-Token",
     *          description="Токен пользователя",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     *      @SWG\Parameter(
     *          in="body",
     *          name="data",
     *          @SWG\Schema(type="object", description="Структура для создания шаблона", required={"type"},
     *              @SWG\Property(property="type", type="string", description="Тип шаблона", enum={"sms", "email", "push"}),
     *              @SWG\Property(property="name", type="string", description="Имя шаблона"),
     *              @SWG\Property(property="message", type="string", description="Содержимое шаблона"),
     *          ),
     *      ),
     * ),
     *
     * Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          -X POST -d'{"type": "email", "name": "registration", "message": "Hello, {{username}}!"}'
     *          'http://localhost/v1/templates'
     *
     * @return JsonResponse
     * @throws DatabaseException
     */
    public function create(): JsonResponse
    {
        $data = $this->getBodyParams();

        $constraints = new Assert\Collection([
            'type' => [
                new Assert\NotBlank(),
                new Assert\NotNull(),
                new Assert\Type(['type' => 'string']),
                new Enum([
                    'enum' => new TemplateEnum()
                ]),
            ],
            'name' => [
                new Assert\NotBlank(),
                new Assert\NotNull(),
                new Assert\Type(['type' => 'string']),
            ],
            'message' => new Assert\Optional([
                new Assert\Type(['type' => 'string']),
            ]),
        ]);

        $this->_validationService->validate(
            $data,
            $constraints,
            new BadRequestException(
                $this->_translator->trans('exceptions.bad_request')
            )
        );

        return $this->json(
            $this->_templateService->create($data)
        );
    }

    /**
     * Возвращает информацию о указанном шаблоне
     *
     * @SWG\Get(
     *      tags={"templates"},
     *      description="Возвращает информацию о указанном шаблоне",
     *      @SWG\Response(
     *          response="200",
     *          description="Информация о шаблоне",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref=@Model(type=App\Entity\Template::class)),
     *          ),
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="header",
     *          name="X-User-Token",
     *          description="Токен пользователя",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="path",
     *          name="guid",
     *          description="Идентификатор шаблона",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     * )
     *
     * Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          'http://localhost/v1/templates/03ebff53-8667-44b5-97d5-f81030558e70'
     *
     * @param string $guid Идентификатор шаблона
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function view(string $guid): JsonResponse
    {
        return $this->json(
            $this->_templateService->view($guid)
        );
    }

    /**
     * Обновляет данные шаблона по его идентификатору
     *
     * @SWG\Put(
     *      tags={"templates"},
     *      description="Обновляет данные шаблона по его идентификатору",
     *      @SWG\Response(
     *          response="404",
     *          description="Шаблон не найден",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="Шаблон не найден", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref="#/definitions/ErrorData"),
     *          ),
     *      ),
     *      @SWG\Response(
     *          response="400",
     *          description="Ошибка обновления",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="Ошибка базы данных", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref="#/definitions/ErrorData"),
     *          ),
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="header",
     *          name="X-User-Token",
     *          description="Токен пользователя",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="path",
     *          name="guid",
     *          description="Идентификатор шаблона",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     *      @SWG\Parameter(
     *          in="body",
     *          name="data",
     *          @SWG\Schema(type="object", description="Структура для создания шаблона",
     *              @SWG\Property(property="type", type="string", description="Тип шаблона", enum={"sms", "email", "push"}),
     *              @SWG\Property(property="name", type="string", description="Имя шаблона"),
     *              @SWG\Property(property="message", type="string", description="Содержимое шаблона"),
     *          ),
     *      ),
     * )
     *
     * Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          -X PUT -d'{"message": "Hi, {{username}}."}'
     *          'http://localhost/v1/templates/03ebff53-8667-44b5-97d5-f81030558e70'
     *
     * @param string $guid Идентификатор шаблона
     * @return JsonResponse
     * @throws ModelNotFoundException
     * @throws DatabaseException
     */
    public function update(string $guid): JsonResponse
    {
        $data = $this->getBodyParams();

        $constraints = new Assert\Collection([
            'type' => new Assert\Optional([
                new Assert\Type(['type' => 'string']),
                new Enum([
                    'enum' => new TemplateEnum()
                ]),
            ]),
            'name' => new Assert\Optional([
                new Assert\Type(['type' => 'string']),
            ]),
            'message' => new Assert\Optional([
                new Assert\Type(['type' => 'string']),
            ]),
        ]);

        $this->_validationService->validate(
            $data,
            $constraints,
            new BadRequestException(
                $this->_translator->trans('exceptions.bad_request')
            )
        );

        return $this->json(
            $this->_templateService->update($guid, $data)
        );
    }

    /**
     * Удаляет шаблон по его идентификатору
     *
     * @SWG\Delete(
     *      tags={"templates"},
     *      @SWG\Response(
     *          response="200",
     *          description="Успешно ли удален шаблон",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="boolean", example=true),
     *          ),
     *      ),
     *      @SWG\Response(
     *          response="404",
     *          description="Шаблон не найден",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="Шаблон не найден", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref="#/definitions/ErrorData"),
     *          ),
     *      ),
     *      @SWG\Response(
     *          response="400",
     *          description="Ошибка удалениея",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean", description="Успешно ли завершился запрос"),
     *              @SWG\Property(property="errno", type="integer", description="Номер ошибки"),
     *              @SWG\Property(property="error", type="string", example="Ошибка базы данных", description="Текст ошибки"),
     *              @SWG\Property(property="data", type="object", ref="#/definitions/ErrorData"),
     *          ),
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="header",
     *          name="X-User-Token",
     *          description="Токен пользователя",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     *      @SWG\Parameter(
     *          type="string",
     *          in="path",
     *          name="guid",
     *          description="Идентификатор шаблона",
     *          required=true,
     *          pattern="^\w{8}\-\w{4}-\w{4}-\w{4}-\w{12}$"
     *      ),
     * )
     *
     * Пример:
     *      curl -H 'X-User-Token: 333ab0e8-66c8-45a7-ba12-6c233c020355' -H 'Content-Type: application/json'
     *          -X DELETE 'http://localhost/v1/templates/03ebff53-8667-44b5-97d5-f81030558e70'
     *
     * @param string $guid Идентификатор шаблона
     * @return JsonResponse
     * @throws ModelNotFoundException
     * @throws DatabaseException
     */
    public function delete(string $guid): JsonResponse
    {
        return $this->json(
            $this->_templateService->delete($guid)
        );
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function permissions(): array
    {
        return [
            'index' => 'get_templates_list',
            'create' => 'create_template',
            'update' => 'update_template',
            'view' => 'view_template',
            'delete' => 'delete_template'
        ];
    }
}
