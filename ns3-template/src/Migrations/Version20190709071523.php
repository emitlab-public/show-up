<?php
declare(strict_types=1);

namespace DoctrineMigrations;

use App\Migrations\AbstractMigration;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Gp\Bundle\Perms\Traits\PermsMigrationTrait;
use Exception;

/**
 * Class Version20190709071523
 * @package DoctrineMigrations
 */
final class Version20190709071523 extends AbstractMigration
{
    use PermsMigrationTrait;

    /**
     * @inheritDoc
     * @return string
     */
    public function getDescription(): string
    {
        return 'Создание базовой структуры';
    }

    /**
     * @inheritDoc
     * @param Schema $schema
     * @throws DBALException
     * @throws Exception
     */
    public function up(Schema $schema): void
    {
        parent::up($schema);

        $this->addSql('CREATE TABLE IF NOT EXISTS templates (
          guid UUID NOT NULL, 
          created TIMESTAMP(0) WITH TIME ZONE NOT NULL, 
          updated TIMESTAMP(0) WITH TIME ZONE NOT NULL, 
          type VARCHAR(255) NOT NULL, 
          name VARCHAR(64) NOT NULL, 
          message TEXT DEFAULT NULL, 
          PRIMARY KEY(guid)
        )');
        $this->addSql('CREATE UNIQUE INDEX IF NOT EXISTS uk_template_type_name ON templates (type, name)');
        $this->addSql('COMMENT ON COLUMN templates.guid IS \'Идентификатор шаблона\'');
        $this->addSql('COMMENT ON COLUMN templates.created IS \'Дата/Время создания записи\'');
        $this->addSql('COMMENT ON COLUMN templates.updated IS \'Дата/Время последней модификации записи\'');
        $this->addSql('COMMENT ON COLUMN templates.type IS \'Тип транспорта\'');
        $this->addSql('COMMENT ON COLUMN templates.name IS \'Имя шаблона\'');
        $this->addSql('COMMENT ON COLUMN templates.message IS \'Текст шаблона\'');
        $this->addSql('CREATE TABLE IF NOT EXISTS perms (
          guid UUID NOT NULL, 
          created TIMESTAMP(0) WITH TIME ZONE NOT NULL, 
          updated TIMESTAMP(0) WITH TIME ZONE NOT NULL, 
          perm_name VARCHAR(255) NOT NULL, 
          role_name VARCHAR(255) NOT NULL, 
          PRIMARY KEY(guid)
        )');
        $this->addSql('CREATE UNIQUE INDEX IF NOT EXISTS perms_perm_name_role_name_key ON perms (perm_name, role_name)');
        $this->addSql('COMMENT ON COLUMN perms.guid IS \'Идентификатор права\'');
        $this->addSql('COMMENT ON COLUMN perms.created IS \'Дата/Время создания записи\'');
        $this->addSql('COMMENT ON COLUMN perms.updated IS \'Дата/Время последней модификации записи\'');
        $this->addSql('COMMENT ON COLUMN perms.perm_name IS \'Имя права\'');
        $this->addSql('COMMENT ON COLUMN perms.role_name IS \'Имя роли\'');

        $this->addPerms([
            'ADMIN' => array_keys([
                'get_available_perms' => 'Получение списка возможных прав',
                'get_perms_list' => 'Получение списка прав',
                'create_perms' => 'Установка прав',
                'update_perms' => 'Добавление прав к существующим',
                'view_perms' => 'Просмотр прав для определенной роли',
                'delete_perm' => 'Удаление прав у роли',

                'get_templates_list' => 'Просмотр списка шаблонов',
                'create_template' => 'Создание шаблона',
                'update_template' => 'Обновление шаблона',
                'view_template' => 'Просмотр шаблона',
                'delete_template' => 'Удаление шаблона'
            ])
        ]);
    }

    /**
     * @inheritDoc
     * @param Schema $schema
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        parent::down($schema);

        $this->addSql('DROP TABLE IF EXISTS templates');
        $this->addSql('DROP TABLE IF EXISTS perms');
    }
}
