<?php
declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\Migrations\AbstractMigration as BaseMigration;
use Doctrine\DBAL\Schema\Schema;
use Exception;

/**
 * Базовый класс миграций
 *
 * TODO: кандидат в Tools либо в отдельную репу
 *
 * @package App\Migrations
 */
abstract class AbstractMigration extends BaseMigration
{
    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->checkPlatform();
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->checkPlatform();
    }

    /**
     * @throws DBALException
     */
    private function checkPlatform(): void
    {
        $platformName = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf(
            $platformName !== 'postgresql',
            'Migration can only be executed safely on \'postgresql\'.'
        );
    }

    /**
     * @param string $prefix
     * @param array $fileNames
     */
    protected function addSqlFromFiles(string $prefix, array $fileNames): void
    {
        $path = __DIR__ . '/sql/' . $prefix . '/';

        foreach ($fileNames as $fileName) {
            $sql = file_get_contents($path . $fileName);
            $this->addSql($sql);
        }
    }

    /**
     * Добавление комментариев
     *
     * @param array $comments Комментарии где ключ - таблица либо колонка, значение - комментарий
     */
    protected function addComments(array $comments): void
    {
        $commentTable = "COMMENT ON TABLE %s IS '%s';";
        $commentColumn = "COMMENT ON COLUMN %s IS '%s'";

        foreach ($comments as $key => $comment) {
            $isColumn = strpos($key, '.') !== false;

            $this->addSql(
                sprintf($isColumn ? $commentColumn : $commentTable, $key, $comment)
            );
        }
    }

    /**
     * Добавление расширений
     *
     * @param array $extensions Массив расширений
     */
    protected function checkExtensions(array $extensions): void
    {
        foreach ($extensions as $check => $extensionName) {
            try {
                $this->connection->exec($check);
            } catch (Exception $e) {
                $message = sprintf('CREATE EXTENSION IF NOT EXISTS "%s";', $extensionName);

                $this->write(
                    sprintf(
                        '<comment>Extension %s not exist!</comment>. Try: %s as root',
                        $extensionName, $message
                    )
                );

                $this->abortIf(
                    true,
                    sprintf('Extension %s not exists!', $extensionName)
                );
            }
        }
    }
}
