<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Template;
use App\Repository\TemplateRepository;
use Doctrine\ORM\NonUniqueResultException;
use App\Exception\{DatabaseException, ModelNotFoundException};
use Gp\Component\EntityHelper\EntityTrait;
use Gp\Component\Pagination\{Pagination, PaginationList};
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment as TwigEnvironment;
use Twig\Error\{LoaderError, RuntimeError, SyntaxError};
use Twig\Loader\ArrayLoader;

/**
 * Сервис для работы с шаблонами
 *
 * @package App\Service
 */
class TemplateService
{
    use EntityTrait;

    /** @var TemplateRepository */
    private $_templateRepository;

    /** @var TranslatorInterface */
    private $_translator;

    /**
     * TemplateService constructor.
     * @param TemplateRepository $templateRepository
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TemplateRepository $templateRepository,
        TranslatorInterface $translator
    )
    {
        $this->_templateRepository = $templateRepository;
        $this->_translator = $translator;
    }

    /**
     * Получение списка шаблонов
     *
     * @param Pagination $pager
     * @return PaginationList
     * @throws NonUniqueResultException
     * @throws DatabaseException
     */
    public function getList(Pagination $pager): PaginationList
    {
        [$totalCount, $items] = $this->_templateRepository->getList($pager);

        return (new PaginationList($pager))
            ->setTotalCount($totalCount)
            ->setItems(
                array_map(function (Template $it) {
                    return $this->getAttributes($it);
                }, $items)
            );
    }

    /**
     * Создание шаблона
     *
     * @param array $data
     * @return array
     * @throws DatabaseException
     */
    public function create(array $data): array
    {
        return $this->getAttributes(
            $this->_templateRepository->create($data)
        );
    }

    /**
     * Просмотр шаблона
     *
     * @param string $guid Идентификатор шаблона
     * @return array
     * @throws ModelNotFoundException
     */
    public function view(string $guid): array
    {
        return $this->getAttributes(
            $this->getTemplateByGuid($guid)
        );
    }

    /**
     * Обновление шаблона
     *
     * @param string $guid Идентификатор записи
     * @param array $data Обновляемая информация
     * @return array
     * @throws ModelNotFoundException
     * @throws DatabaseException
     */
    public function update(string $guid, array $data): array
    {
        return $this->getAttributes(
            $this->_templateRepository->update(
                $this->getTemplateByGuid($guid),
                $data
            )
        );
    }

    /**
     * Удаление шаблона
     *
     * @param string $guid Идентификатор шаблона
     * @return bool
     * @throws ModelNotFoundException
     * @throws DatabaseException
     */
    public function delete(string $guid): bool
    {
        $this->_templateRepository->remove(
            $this->getTemplateByGuid($guid)
        );

        return true;
    }

    /**
     * Получение шаблона по идентификатору
     *
     * @param string $guid Идентификатор шаблона
     * @return Template
     * @throws ModelNotFoundException
     * @throws DatabaseException
     */
    public function getTemplateByGuid(string $guid): Template
    {
        $result = $this->_templateRepository->find($guid);

        if ($result === null) {
            throw new ModelNotFoundException(
                $this->_translator->trans('exceptions.entity.template_not_found')
            );
        }

        return $result;
    }

    /**
     * Получение шаблона по имени и типу
     *
     * @param string $type Тип шаблона
     * @param string $name Имя шаблона
     * @return Template
     * @throws ModelNotFoundException
     * @throws DatabaseException
     */
    public function getTemplateByTypeName(string $type, string $name): Template
    {
        $result = $this->_templateRepository->findOneBy([
            'name' => $name,
            'type' => $type
        ]);

        if ($result === null) {
            throw new ModelNotFoundException(
                $this->_translator->trans('exceptions.entity.template_not_found')
            );
        }

        return $result;
    }

    /**
     * Отрисовка шаблона
     *
     * @param string $type Тип шаблона
     * @param string $name Имя шаблона
     * @param array $params Параметры для шаблона
     * @return string
     * @throws ModelNotFoundException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws DatabaseException
     */
    public function render(string $type, string $name, array $params): string
    {
        $model = $this->getTemplateByTypeName($type, $name);

        $templateName = sprintf('%s_%s', $type, $name);

        $loader = new ArrayLoader([
            $templateName => $model->message
        ]);

        $twig = new TwigEnvironment($loader, [
            'cache' => false,
            'auto_reload' => false
        ]);

        return $twig->render($templateName, $params);
    }
}
