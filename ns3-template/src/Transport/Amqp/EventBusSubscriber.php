<?php
declare(strict_types=1);

namespace App\Transport\Amqp;

use Gp\Bundle\Amqp\Interfaces\AmqpMessageBusInterface;
use Gp\Bundle\EventBus\Interfaces\EventBusSubscriberInterface;

/**
 * Транспорт-подписчик, обеспечивающий подписку на одно событие в amqp шине.
 *
 * @package App\Transport\Amqp
 */
class EventBusSubscriber implements EventBusSubscriberInterface
{
    /** @var AmqpMessageBusInterface */
    private $_transport;

    /**
     * EventBusSubscriber constructor.
     * @param AmqpMessageBusInterface $amqpMessageBus
     */
    public function __construct(AmqpMessageBusInterface $amqpMessageBus)
    {
        $this->_transport = $amqpMessageBus;
    }

    /**
     * Подписывает callback функцию на определенное событие
     *
     * @param string $event Событие, по которому нужно слушать сообщения. Пример: 'users.profile.updated'
     * @param callable $consumer Функция, котороая будет вызываться при получении сообщения. В качестве аргумента получает сообщение в виде массива.
     */
    public function subscribe(string $event, callable $consumer): void
    {
        $this->_transport->subscribe($event, $consumer);
    }
}
