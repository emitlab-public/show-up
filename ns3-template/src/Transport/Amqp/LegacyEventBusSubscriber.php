<?php
declare(strict_types=1);

namespace App\Transport\Amqp;

use ErrorException, Exception;
use Gp\Bundle\Amqp\Service\AmqpBusConnectionService;
use Gp\Bundle\EventBus\Interfaces\EventBusSubscriberInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

/**
 * Транспорт-подписчик, обеспечивающий подписку старого формата на одно событие  в amqp шине.
 * @deprecated
 *
 * @package App\Transport\Amqp
 */
class LegacyEventBusSubscriber implements EventBusSubscriberInterface
{
    /** @var AmqpBusConnectionService */
    private $_amqpBus;

    /** @var LoggerInterface */
    private $_logger;

    /** @var string */
    private $_configName;

    /**
     * LegacyEventBusSubscriber constructor.
     * @param string $configName
     * @param AmqpBusConnectionService $amqp
     * @param LoggerInterface $logger
     */
    public function __construct(string $configName, AmqpBusConnectionService $amqp, LoggerInterface $logger)
    {
        $this->_amqpBus = $amqp;
        $this->_logger = $logger;
        $this->_configName = $configName;
    }

    /**
     * Подписывает callback функцию на определенное событие
     *
     * @param string $event Событие, по которому нужно слушать сообщения. Пример: 'refs::sync.update.geo::refs_es_sync_queue'
     * @param callable $consumer Функция, котороая будет вызываться при получении сообщения. В качестве аргумента получает сообщение в виде массива.
     * @throws ErrorException
     * @throws Exception
     */
    public function subscribe(string $event, callable $consumer): void
    {
        $connection = $this->_amqpBus->getNewConnection($this->_configName);

        $channel = $connection->channel();

        [$exchangeName] = explode('::', $event);

        $queueName = $event;

        $channel->exchange_declare($exchangeName, AMQP_EX_TYPE_DIRECT, false, true, false);

        $args['x-dead-letter-exchange'] = ['S', 'ns2-dlx'];

        $channel->queue_declare(
            $queueName,
            false,
            true,
            false,
            false,
            false,
            $args
        );

        // Здесь ключ маршрутизации не используеся, хоть и direct
        $channel->queue_bind($queueName, $exchangeName);

        $logger = $this->_logger;

        // Функция с промежутойчной обработкой сообщения
        $wrapper = static function (AMQPMessage $msg) use ($consumer, $logger) {
            /**
             * @var AMQPChannel $ch
             */
            $ch = !empty($msg->delivery_info['channel']) ? $msg->delivery_info['channel'] : null;
            $deliveryTag = !empty($msg->delivery_info['delivery_tag']) ? $msg->delivery_info['delivery_tag'] : null;
            $routingKey = !empty($msg->delivery_info['routing_key']) ? $msg->delivery_info['routing_key'] : null;

            $data = json_decode($msg->getBody(), true);

            $consumerName = $consumer[1];

            $logger->info('[Legacy] Get message', ['routing_key' => $routingKey, 'consumer' => $consumerName, 'message' => $data]);

            try {
                $result = $consumer($data, $routingKey);
            } catch(Exception $e) {
                $logger->error($e->getMessage(), ['data' => $data, 'routing_key' => $routingKey]);
                $result = false;
            }

            if ($result) {
                $logger->info('[Legacy] Message is acked', ['routing_key' => $routingKey, 'consumer' => $consumerName, 'message' => $data]);
                $ch->basic_ack($deliveryTag);
            } else {
                $logger->warning('[Legacy]  Message is nacked', ['routing_key' => $routingKey, 'consumer' => $consumerName, 'message' => $data]);
                $ch->basic_nack($deliveryTag);
            }
        };

        $channel->basic_qos(0, 5, true);

        $channel->basic_consume(
            $queueName,
            '',
            false,
            false,
            false,
            false,
            $wrapper
        );

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    /**
     * Упаковывает массив в AMQPMessage
     *
     * @param string|array $message
     * @return AMQPMessage
     */
    private function toAmqpMessage($message): AMQPMessage
    {
        return new AMQPMessage(
            is_array($message) ? json_encode($message) : $message,
            [
                'content_type' => 'application/json',
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]
        );
    }
}
