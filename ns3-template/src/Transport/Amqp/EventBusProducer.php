<?php
declare(strict_types=1);

namespace App\Transport\Amqp;

use Gp\Bundle\Amqp\Interfaces\AmqpMessageBusInterface;
use Gp\Bundle\EventBus\Interfaces\EventBusProducerInterface;

/**
 * Транспорт-продюсер, обеспечивающий транспорт сообщений по amqp шине
 *
 * @package App\Transport\Amqp
 */
class EventBusProducer implements EventBusProducerInterface
{
    /**
     * @var AmqpMessageBusInterface
     */
    private $_transport;

    /**
     * EventBusProducer constructor.
     * @param AmqpMessageBusInterface $amqpMessageBus
     */
    public function __construct(AmqpMessageBusInterface $amqpMessageBus)
    {
        $this->_transport = $amqpMessageBus;
    }

    /**
     * Отправляет сообщение по определенному событию
     *
     * @param string $event Событие, по которому отправиться сообщение. Пример: 'users::profile.updated'
     * @param array $data Тело сообщения
     */
    public function publish(string $event, array $data): void
    {
        $this->_transport->publish($event, $data);
    }
}
