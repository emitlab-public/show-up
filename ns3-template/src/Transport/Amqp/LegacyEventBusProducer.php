<?php
declare(strict_types=1);

namespace App\Transport\Amqp;

use Exception;
use Gp\Bundle\Amqp\Service\AmqpBusConnectionService;
use Gp\Bundle\EventBus\Interfaces\EventBusProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

/**
 * Транспорт-продюсер, обеспечивающий поддержку старого формата отправки сообщений по amqp шине
 *
 * TODO: удалить, когда полностью перейдем на новый формат очередей
 * @deprecated
 * @package App\Transport\Amqp
 */
class LegacyEventBusProducer implements EventBusProducerInterface
{
    /** @var AmqpBusConnectionService */
    private $_amqpBus;

    /** @var LoggerInterface */
    private $_logger;

    /** @var string */
    private $_configName;

    /**
     * LegacyEventBusProducer constructor.
     * @param string $configName
     * @param AmqpBusConnectionService $amqp
     * @param LoggerInterface $logger
     */
    public function __construct(string $configName, AmqpBusConnectionService $amqp, LoggerInterface $logger)
    {
        $this->_amqpBus = $amqp;
        $this->_logger = $logger;
        $this->_configName = $configName;
    }

    /**
     * Отправляет сообщение по определенному событию по старому формату
     *
     * @param string $event Событие, по которому отправиться сообщение. Должно соответствовать формату: <exchangeName>::<routingKey>
     * @param array $data Тело сообщения
     */
    public function publish(string $event, array $data): void
    {
        try {
            [$exchangeName, $routingKey] = explode('::', $event);

            $channel = $this->_amqpBus->getStableConnection($this->_configName)->channel();

            $channel->exchange_declare($exchangeName, AMQP_EX_TYPE_TOPIC, false, true, false);

            $msg =  $this->toAmqpMessage($data);

            $channel->basic_publish($msg, $exchangeName, $routingKey);

        } catch (Exception $e) {

            $params = [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode()
            ];

            $this->_logger->error($e->getMessage(), $params);
        }
    }

    /**
     * Упаковывает массив в AMQPMessage
     *
     * @param string|array $message
     * @return AMQPMessage
     */
    private function toAmqpMessage($message): AMQPMessage
    {
        return new AMQPMessage(
            is_array($message) ? json_encode($message) : $message,
            [
                'content_type' => 'application/json',
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]
        );
    }
}
