# Тестовый пример проекта на Symfony

Данный репозиторий показывает пример написания код на Symfony

А так же - пример файлов которые должны быть в проекте

## Установка

Для работы необходимо:

* php7.2 и выше
  * amqp
  * bcmath
  * mbstring
  * json
  * curl
  * zip
  * http
  * ctype
  * iconv
* composer 1.8.5 и выше

## Разработка

Для переопределения `.env` параметров используется файл `.env.local`

Перед коммитом необходимые параметры необходимо внести в `.env` и в `.env.test` если необходимо.

### WebServer

Самым лучшим вариантом будет использовать nginx и на основе конфига `host.nginx.conf` создать новый хост.

Однако Symfony позволяет запустить собственный сервер

```bash
php bin/console server:run
``` 

По-умолчанию он будет работать на порту 8000 (http://localhost:8000)

### Валидаторы

Про валидаторы читаем [тут](https://symfony.com/doc/current/forms)

### Локальная установка бандла

Приведу сухой пример

```json
{
  "gp/perms-bundle": "dev-develop"
}
```

Где develop - имя текущей ветки
Например - feature/NS2-1000 будет как dev-feature/NS2-1000

```json
{
  "repositories": [
    {
      "type": "path",
      "url": "/home/sai/www/ns2/bundles/gp-perms",
      "options": {
        "symlink": true
      }
    }
  ]
}
```

## IDE

Для работы используется IDE [PhpStorm](https://www.jetbrains.com/phpstorm/)

Совместно с плагинами:

  * [Symfony](https://plugins.jetbrains.com/plugin/7219-symfony-plugin) ([github](https://github.com/Haehnchen/idea-php-symfony2-plugin-doc))
  * [EditorConfig](https://plugins.jetbrains.com/plugin/7294-editorconfig)
  * [Php Inspections](https://plugins.jetbrains.com/plugin/7622-php-inspections-ea-extended-)

## Тестирование

Команда для запуска тестов

```bash
./vendor/bin/codecept run
```

Данная команда так же сгенерирует покрытие кода, которое можно будет найти в папке
`var/coverage`. Открывать через браузер и смотреть как много кода покрыто тестами

### База данных

Для тестирования необходимо добавить тестовую базу данных.
По-умолчанию, во всех проектах будет браться имя текущей БД и добавляться постфикс `test`

Для данного микросервиса имя бд будет -> `ns3_templates_test`

Чтобы накатить миграции на тестовую БД 

```bash
export APP_ENV=test; php bin/console doctrine:migrations:migrate
```

Пример создания дампа

```bash
pg_dump -c --if-exists --inserts -f /home/sai/www/ns3/ns3-template/tests/dump/2019-06-08_full.sql ns3_templates
```
